<?php

namespace app\manage\controller;

use app\model\Admin;
use think\facade\Cache;
use think\Db;
class Home extends Base
{
    /**
     *  首页
     */
    public function index(){

        
        return view("index");
    }
    /**
     *  欢迎页
     */
    public function welcome(){
        //订单统计
        $info = Db::name("platformh_statistical")->where("create_time",date("Y-m-d",time()))->find();
        //下发统计
        $with = Db::name("with")->where("create_time",date("Y-m-d",time()))->find();
       
        $this->assign("info",$info);
        $this->assign("with",$with);
        
        return view("welcome");
    }
    /**
     * Undocumented 清空缓存
     *
     * @return void
     */
    public function webCache(){
        $CACHE_PATH = config('cache.runtime_path').'/cache/';
        $TEMP_PATH = config('cache.runtime_path').'/temp/';
        $LOG_PATH = config('cache.runtime_path').'/log/';
        if(delete_dir_file($CACHE_PATH) && delete_dir_file($LOG_PATH) && delete_dir_file($TEMP_PATH)){
            //redis清空
            Cache::clear();
            $redis = new \Predis\Client();
            $redis->FLUSHALL();
            return $this->success("清除成功");
        }else{
            return $this->error("清除失败");
        }
        
    }
    public function  google(){
        $admin_auth = Session("auth_admin");
        if(empty($admin_auth)){
            return $this->error('登录超时','/manage/login');
        }
        $info = Admin::where("id",$admin_auth['auth_admin_id'])->find();
        if($info){
            $info->googl_secret = $this->request->param("google_secret");
            $info->save();
            get_one_insert('auth_admin',$admin_auth['auth_admin_id'],$info);
        }else{
            return $this->error('数据不存在');
        }
        return $this->success("绑定成功",'/manage');
        
    }







}