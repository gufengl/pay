<?php

namespace app\manage\controller;

use think\Db;
use \app\model\Order as Orders;
use \app\model\Mch;
use \app\model\Channel;
class Order extends Base{
    /**
     * Undocumented 构造方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->assign("data",$this->request->param());
        $this->assign("payment",config("custom.PAYMENT_TYPE"));
        $redis = new \Predis\Client();
        $list = $redis->zrange('channel',0,-1);
        if($list){
            foreach ($list as $key => $value) {
                $list[$key] = json_decode($value,true);
            }
        }else{
            $list = Channel::where("status",1)->field("id,title")->select();
        }
        $this->assign("channel",$list);
        
    }
    /**
     * Undocumented 订单列表
     *
     * @return void
     */
    public function index(){
        
        $list = Orders::order('id','desc')->where(function($query){
           
            //订单状态
            $pay_status = input("pay_status");
            if(!empty($pay_status)){
                $query->where("pay_status",$pay_status);
            }
            //回调状态
            $notice = input("notice");
            if(!empty($notice)){
                $query->where("notice",$notice);
            }
            //渠道名称
            $channel = input("channel");
            if(!empty($channel)){
                $query->where("channel_id",$channel);
            }
            //支付类型
            $payment = input("payment");
            if(!empty($payment)){
                $query->where("pay_code",$payment);
            }
            //金额
            $min_amount = input("min_amount");
            $max_amount = input("max_amount");
            if(!empty($payment) && !empty($max_amount)){
                $query->where("amount",'>=',$min_amount)->whereOr("amount",'<=',$max_amount);
            }
            //商户单号/系统单号
            $keyword = input("keyword");
            if(!empty($keyword)){
                $query->where("out_trade_no|systen_no",'like','%'.$keyword.'%');
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:059',time()));
            if(!empty($start) && !empty($end)){
                $query->where("create_time",'between',[$start,$end]);
            }
        })
        ->field("id,mch_id,out_trade_no,systen_no,amount,actual_amount,total_fee,channel_id,pay_code,pay_status,notice,create_time")
        ->paginate(10,false,['query' =>$this->request->param()]);
        //导出
        if(!empty($this->request->param('order_export')) && $this->request->param('order_export') == 'export'){
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'下载订单报表');
            $this->order_export($this->request->param());
        }
        //统计
        //$this->orderTotal($this->request->param());
        $statistical = [];
        $page = $list->render();
        //$this->assign("statistical",$statistical);
        $this->assign("list",$list);
        $this->assign("page",$page);
        return view("index");
    }
    /**
     * Undocumented 订单详情
     *
     * @return void
     */
    public function details(){
        $id = $this->request->param("oid");
        $info = orders::where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        $this->assign("info",$info);
        return view("details");
    }
    
    /**
     * Undocumented 补发通知
     *
     * @return void
     */
    public function notice(){
        $id = $this->request->param("oid");
        $info = orders::mch_order(['id'=>$id],"id,mch_id,notice,goods,out_trade_no,systen_no,amount,actual_amount,pay_time,pay_status,callback_url");
        if(!$info){
            return $this->error("数据不存在");
        }
        if($info->pay_status != 2){
            return $this->error("订单未支付或者订单已关闭，不能补发");
        }
        //获取商户信息
        $mch = Mch::get_mch($info->mch_id);
        if(!$mch){
            return $this->error("商户不存在");
        }
        //组装回调数据
        $native = [
            "mch_id" => $info->mch_id,//商户号
            "out_trade_no" => $info->out_trade_no,//商户单号
            "systen_no" => $info->systen_no,//系统单号
            "amount" => $info->amount, //下单金额
            "actual_amount" => $info->actual_amount,//实际支付
            "pay_time" => strtotime($info->pay_time),//支付时间
        ];
        //签名
        $native['sign'] = get_sign($native,$mch->md5key);
        //发送curl异步回调
        $result = orders::get_notify($native,$info->callback_url);
        if($result == 'success'){
            if($info->notice == 1){
                $info->notice = 2;
                $info->save();
            } 
        }
        getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'给商户['.$info->mch_id.']补发通知,订单号:['.$info->systen_no.']');
        return $this->success("发送成功,返回：".$result,'',['notice'=>$info->notice,'result'=>$result]);
    }
    /**
     * Undocumented 强制入账
     *
     * @return void
     */
    public function compel(){
        $id = $this->request->param("oid");

        $info = orders::mch_order(['id'=>$id],"id,pay_status,amount,remark,notice,mch_id,settle,systen_no,is_manual");
        if(!$info){
            return $this->error("订单不存在");
        }
        if($info->pay_status != 1){
            return $this->error("订单已支付或已关闭");
        }
        $result = orders::order_update($info->id,$info->amount,6);
        if($result['code'] != '1'){
            return $this->error($result['msg']);
        }
        $info->remark = '强制入账';
        $info->is_manual = 2;
        $info->save();
        //插入操作日志
        getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'给商户['.$info->mch_id.']强制入账'.$info->settle.'元,订单号:['.$info->systen_no.']');
        return $this->success('强制入账成功,异步通知返回:'.$result['msg']);
    }
    /**
     * Undocumented 渠道统计
     *
     * @return void
     */
    public function chan_statistical(){
        
        $list = Db::name("chan_statistical")->where(function($query){
            $channel = input('channel');
            if(!empty($channel)){
                $query->where('channel_id',$channel);
            }
            $start = input('start',date('Y-m-d',time()));
            if(!empty($start)){
                $query->where('create_time',$start);
            }
        })->paginate(10,false,['query' =>$this->request->param()]);
        //导出
        if(!empty($this->request->param('chan_export')) && $this->request->param('chan_export') == 'export'){
            $this->chan_export($this->request->param());
        }
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("statistical/channel");
    }
    /**
     * Undocumented 商户统计
     *
     * @return void
     */
    public function mch_statistical(){

        $list = Db::name("mch_statistical")->where(function($query){
            $keyword = input("keyword");
            if(!empty($keyword)){
                $mch = Mch::where('mch|mch_name','like','%'.$keyword.'%')->column('mch');
                if(!empty($mch)){
                    $query->where('mch_id','in',$mch);
                }
            }
            $time = input("start",date("Y-m-d",time()));
            $query->where('create_time',$time);
        })->paginate(10,false,['query' =>$this->request->param()]);
        //导出
        if(!empty($this->request->param('mch_export')) && $this->request->param('mch_export') == 'export'){
            $this->mch_export($this->request->param());
        }
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("statistical/mch");

    }
    /**
     * Undocumented 平台统计
     *
     * @return void
     */
    public function platformh_statistical(){

        $list = Db::name("platformh_statistical")->where(function($query){
            $time = input("start",date("Y-m-d",time()));
            
            $query->where('create_time',$time);
        })->paginate(10,false,['query' =>$this->request->param()]);
        $page = $list->render();
        //导出
        if(!empty($this->request->param('platform_export')) && $this->request->param('platform_export') == 'export'){
            $this->platform_export($this->request->param());
        }
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("statistical/platform");
    }

}