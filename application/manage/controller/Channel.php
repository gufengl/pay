<?php

namespace app\manage\controller;

use app\model\Channel as Chan;

use think\Db;

class Channel extends Base
{
    /**
     * Undocumented 构造方法
     */
    public function __construct()
    {
        parent::__construct();
       
        $this->assign("data",$this->request->param());
        $redis = new \Predis\Client();
        $list = $redis->zrange('channel',0,-1);
        if($list){
            foreach ($list as $key => $value) {
                $list[$key] = json_decode($value,true);
            }
        }else{
            $list = Chan::where("status",1)->field("id,title")->select();
        }
        
        $this->assign("channel",$list);
        
        $this->assign("paymentType",config("custom.PAYMENT_TYPE"));
    }
    /**
     * Undocumented 渠道列表
     *
     * @return void
     */
    public function index(){

        $list = Chan::order("id","desc")->paginate(10);
        $page = $list->render();
        $this->assign("list",$list);
        $this->assign("page",$page);
        return view("index");
    }
    /**
     * Undocumented 编辑渠道
     *
     * @return void
     */
    public function channel_edit(){
       
        $data = $this->request->param();
        if(isset($data['id'])){//更新
            $result = Chan::channel_creat($data);
            if($result === true){
                getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'更新渠道ID['.$data['id'].']信息');
                return $this->success("修改成功","/manage/channel");
            }else{
                return $this->error("修改失败");
            }
        }else{
            $info = Chan::where("id",$data['cid'])->find();
            if(!$info){
                $this->error("数据不存在");
            }
            $this->assign("info",$info);
        }
        
        return view("edit");
    }
    /**
     * Undocumented 支付类型列表
     *
     * @return void
     */
    public function payType(){
        
        $list = Db::name("payment")->order("id",'desc')->where(function($query){
            $channel_id = input("channel_id",'');
            if(!empty($channel_id)){
                $query->where("channel_id",$channel_id);
            }
            $paytype_id = input("paytype_id");
            if(!empty($paytype_id)){
                $query->where("pay_type",$paytype_id);
            }
        })->paginate(10,false,['query' =>$this->request->param()]);
      
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        
        return view("payment");
    }
    /**
     * Undocumented 添加/编辑支付类型
     *
     * @return void
     */
    public function addType(){
        $tid = $this->request->param("tid");
        if($this->request->isAjax()){//添加/编辑
            $data = $this->request->param();
            if(empty($data['pay_id'])){
                if(Db("payment")->where("channel_id",$data['channel_id'])->where("pay_type",$data['payment'])->find()){
                    return $this->error("该渠道已配置过此支付类型");
                }
            }
            $result = Chan::payment($data);
            if($result === true){
                $name = Chan::channel_name($data['channel_id']);
                $pay_type = Chan::paymentType($data['payment']);
                if(!empty($data['pay_id'])){//更新
                    getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'更新支付类型ID['.$data['pay_id'].']信息');
                }else{//创建
                    getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'创建支付类型['.$name.'-'.$pay_type.']的渠道');
                }
                return $this->success("操作成功",'/manage/payment');
            }else{
                return $this->error($result);
            }
        }
        $info = array(); 
        if($tid){
            $info = get_one("payment",$tid);
            if(empty($info)){
                $info = Db::name("payment")->where("id",$tid)->find();
            }
        }
        $this->assign("info",$info);
        return view("add_type");
    }
    /**
     * Undocumented 更新支付类型状态
     *
     * @return void
     */
    public function typeStart(){
        $pay_id = $this->request->param("mid");
        $info = Db::name("payment")->where("id",$pay_id)->find();
        if(empty($info)){
            return $this->error("数据不存在");
        }
        $status = $info['status'] == 1 ? 2 : 1;
        $type = $info['status'] == 1 ? '渠道已关闭' : '渠道已开启';
        $result = Db::name("payment")->where("id",$pay_id)->update(array("status" =>$status));
        if($result){
            $list = Db::name("payment")->select();
            get_list_insert('payment',$list);
            Chan::mch_conf();
            $name = Chan::channel_name($info['channel_id']);
            $pay_type = Chan::paymentType($info['pay_type']);
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'更新渠道['.$name.'-'.$pay_type.']状态:'.$type);
            return $this->success($type);
        }else{
            return $this->error("更新失败");
        }
    }
    /**
     * Undocumented 更新渠道状态
     *
     * @return void
     */
    public function channel_status(){
        $id = $this->request->param("mid");
        $info = Chan::where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        $type = $info->status == 1 ? '关闭' : '开启';
        $info->status = $info->status == 1 ? '2' : '1';
        $info->save();
        $list = Chan::field("id,title,code,status,white_ip")->select();
        get_list_insert('channel',$list);
        
        getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'更新渠道['.$info->title.']状态为:'.$type);
        return $this->success("操作成功");
    }
    /**
     * Undocumented 添加渠道
     *
     * @return void
     */
    public function addChannel(){
        $data = $this->request->param();
        
        $result = Chan::channel_creat($data);
        
        if($result === true){
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'添加一条支付渠道['.$data['name'].']');
            return $this->success("添加成功");
        }else{
            return $this->error($result);
        }
    }
    /**
     * Undocumented 删除渠道
     *
     * @return void
     */
    public function del_channel(){
        $id = $this->request->param("cid");
        $info = Chan::where("id",$id)->find();
        if(Db::name("payment")->where("channel_id",$info->id)->find()){
            return $this->error("请先删除支付类型里面相关的配置");
        }
        get_one_del("channel",$id);
        get_all_del('channel_type');
        get_all_del('mch_channel');
        get_all_del('payment');
        $list = Db::name("payment")->select();
        if($list){
            //重新导入redis
            get_list_insert('payment',$list);
        }
        $title = $info->title;
        $info->delete();
        getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'删除一条支付渠道['.$title.']');
        return $this->success("删除成功");
    }
    /**
     * Undocumented 删除支付类型
     *
     * @return void
     */
    public function del_payment(){
        $id = $this->request->param("pid");
        $info = Db::name("payment")->where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        //删除redis里面的数据
        get_one_del("payment",$id);
        get_all_del('channel_type');
        get_all_del('mch_channel');
        Db::name("payment")->where("id",$id)->delete();
        //创建日志
        $name = Chan::channel_name($info['channel_id']);
        $pay_type = Chan::paymentType($info['pay_type']);
        getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'删除一条支付渠道['.$name.'-'.$pay_type.']');
        return $this->success("删除成功");
    }









}