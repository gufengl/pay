<?php

namespace app\manage\controller;

use think\Db;
use app\model\Channel;
use app\model\Mch;

class User extends Base{

    /**
     * Undocumented 构造方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->assign("data",$this->request->param());
       
        $list = Mch::where("is_agent",2)->field("id as auth_admin,mch_name,mch")->select();
        
        $this->assign("agent",$list);
    }
    /**
     * Undocumented 商户列表
     *
     * @return void
     */
    public function index(){
       
        $list = Mch::order("id","desc")->where("is_agent",1)->where(function($query){
            //状态
            $status = input("status",'');
            if(!empty($status)){
                $query->where("status",$status);
            }
            //代理
            $agent_id = input("agent_id",'');
            if(!empty($agent_id)){
                $query->where("agent_id",$agent_id);
            }
            //入金
            $rukuan = input("rukuan",'');
            if(!empty($rukuan)){
                $query->where("rukuan",$rukuan);
            }
            //出款
            $chukuan = input("chukuan",'');
            if(!empty($chukuan)){
                $query->where("chukuan",$chukuan);
            }
            //商户号/商户名称
            $keyword = trim(input("keyword",''));
            if(!empty($keyword)){
                $query->where("mch_name|mch",'like','%'.$keyword.'%');
            }
        })->field("id,mch,mch_name,md5key,balance,freeze,status,rukuan,chukuan,agent_id,create_time")
        ->paginate(10,false,['query' =>$this->request->param()]);
        $page = $list->render();
        
        $total = Mch::statistical();
        
        $this->assign("list",$list);
        $this->assign("total",$total);
        $this->assign("page",$page);
        return view("index");
    }
    /**
     * Undocumented 编辑商户
     *
     * @return void
     */
    public function editMch(){
        $data = $this->request->param();
        if(isset($data['mid']) && !empty($data['mid'])){//修改
            $result = Mch::updateMch($data,$this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id']);
            if($result === true){
                if(!isset($data['agent_id'])){
                    return $this->error("修改成功","/manage/user/agent");
                }else{
                    return $this->error("修改成功","/manage/user");
                }
            }else{
                return $this->error("修改失败");
            }
        }else{
            $info = Mch::where("id",$data['id'])->find();
            if(!$info){
                return $this->error("数据不存在");
            }
            $this->assign("info",$info);
            return view("edit");
        }
    }
    /**
     * Undocumented 创建商户
     *
     * @return void
     */
    public function addMch(){
        $data = $this->request->param();
        $result = Mch::mch_creat($data,$this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id']);
        if($result){
            return $this->success('创建成功','/manage/user');
        }else{
            return $this->error("创建失败");
        }
    }
    public function google(){
        
        return view("google");
    }
    /**
     * Undocumented 更新商户状态
     *
     * @return void
     */
    public function statusMch(){
        $id = $this->request->param("mid");
        $type = $this->request->param("type");
        $info = Mch::where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        $result = Mch::save_mch($type,$info);
        if($result === true){
            switch($type){
                case 'rj'://入金
                    $action = $info->rukuan == 1 ? '修改商户号['.$info->mch.']商户入金状态为:禁用' : '修改商户号['.$info->mch.']商户入金状态为:开启';
                break;
                case 'rj'://入金
                    $action = $info->rukuan == 1 ? '修改商户号['.$info->mch.']商户入金状态为:禁用' : '修改商户号['.$info->mch.']商户入金状态为:开启';
                break;
                default:  //商户状态
                $action = $info->status == 1 ? '修改商户号['.$info->mch.']商户状态为:禁用' : '修改商户号['.$info->mch.']商户状态为:开启';
            }
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],$action);
            return $this->success("操作成功");
        }else{
            return $this->error($result);
        }
    }
    /**
     * Undocumented 代理配置渠道
     *
     * @return void
     */
    public function agentConf(){
        if($this->request->isAjax()){
            $act = $this->request->param('act');
            $agent_id = $this->request->param('mch');
            if($act){
                $pid = array();
                foreach ($act as $key => $value) {
                    if(!empty($value['payment_id'])){
                        $pid[] =$value['payment_id'];
                        $info = Db::name("agent_channel")->where('agent_id',$agent_id)->where("payment_id",$value['payment_id'])->find();
                        if(!$info){//添加
                            Db::name("agent_channel")->insert(array(
                                "agent_id"   => $agent_id,
                                "channel_id" =>$value['channel_id'],
                                "payment_id" =>$value['payment_id']
                            ));
                        }
                    }
                }
                if(!empty($pid)){//删除
                    Db::name("agent_channel")->where('agent_id',$agent_id)->where('payment_id','not in',$pid)->delete();
                }else{//清空
                    Db::name("agent_channel")->where('agent_id',$agent_id)->delete();
                }
            }
            return $this->success("操作成功");
        }else{
           
            $info = Mch::where("id",$this->request->param('mid'))->field("mch,id,agent_id")->find();
            if(!$info){
                return $this->error("数据不存在");
            }
            $redis = new \Predis\Client();
            $list = $redis->get('channel_type');
            if( empty($list)){
                $list = Channel::field("id,title")->select()->toArray();
                foreach($list as $k => $v){//遍历所有渠道
                    //查询渠道配置的支付类型
                    $lists = Db::name("payment")->where("channel_id",$v['id'])->select();
                    if(!empty($lists)){
                        $list[$k]['list'] = $lists;
                        $list[$k]['count'] = count($lists);//统计总数
                    }else{
                        unset($list[$k]);//删除空的数据
                    }
                }
                $redis->set('channel_type',json_encode($list));
            }else{
                $list = json_decode($list,true);
            }
            $info->payment_id = Db::name("agent_channel")->where('agent_id',$info->mch)->column("payment_id");
            $this->assign("list",$list);
            $this->assign("info",$info);
         
            return view("agent_conf");
        }
    }
    /**
     * Undocumented 变更余额
     *
     * @return void
     */
    public function changeAmount(){
        $data = $this->request->param();
        
        $info = Mch::where("id",$data['mch_id'])->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        $result = Mch::editBalance($data,$info->mch,$info->balance,$info->freeze,$info->is_agent);
        if($result === true){
            if($data['type'] == 1){//加/减
                $action = $data['status'] == 1 ? '给商户['.$info->mch.']增加余额'.$data['balance'].'元' : '给商户['.$info->mch.']减少余额'.$data['balance'].'元';
            }else{//解冻/冻结
                $action = $data['status'] == 1 ? '给商户['.$info->mch.']冻结金额'.$data['balance'].'元' : '给商户['.$info->mch.']解冻金额'.$data['balance'].'元';
            }
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],$action);

            return $this->success("操作成功");
        }else{
            return $this->error($result);
        }
    }
    /**
     * Undocumented 删除商户
     *
     * @return void
     */
    public function mch_delete(){
        $mch_id = $this->request->param('mch_id');
        $info = Mch::where("id",$mch_id)->find();
        if(!$info){
            return $this->error("商户不存在");
        }
        get_one_del('mch',$info->mch);//删除redis中的记录
        Db::name("order")->where("mch_id",$info->mch)->delete();//订单
        Db::name("withdrawal")->where("mch_id",$info->mch)->delete();//出款
        Db::name("mch_record")->where("mch_id",$info->mch)->delete();//收益
        Db::name("mch_channel")->where("mch_id",$info->mch)->delete();//配置
        Db::name("bank_card")->where("mch_id",$info->mch)->delete();//银行卡
        $info->delete();
        return $this->success('删除成功');

    }
    /**
     * Undocumented 配置渠道
     *
     * @return void
     */
    public function mchChannel(){
        if($this->request->isAjax()){//配置渠道
            $data = $this->request->param();
            $result = Mch::mch_config($data);
            if($result === true){
                return $this->success("操作成功");
            }else{
                return $this->error($result);
            }
        }
        $mch_id = $this->request->param("mid");
        $info = Mch::where("id",$mch_id)->field("mch,id,agent_id")->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        $redis = new \Predis\Client();
        $list = $redis->get('channel_type');
        if( empty($list)){
            $list = Channel::field("id,title")->select()->toArray();
            foreach($list as $k => $v){//遍历所有渠道
                //查询渠道配置的支付类型
                $lists = Db::name("payment")->where("channel_id",$v['id'])->select();
                if(!empty($lists)){
                    $list[$k]['list'] = $lists;
                    $list[$k]['count'] = count($lists);//统计总数
                }else{
                    unset($list[$k]);//删除空的数据
                }
            }
            $redis->set('channel_type',json_encode($list));
        }else{
            $list = json_decode($list,true);
        }
        $arr = array();
        $at = get_list('mch_channel',$info->mch);
        if($at){
            foreach ($at as  $value) {
                $arr[] = $value['payment'];
            }
        }
        $type = array();
        foreach ($arr as  $value) {
            $t = explode("|",$value);
            foreach ($t as  $v) {
                $s = explode(":",$v);
                $type[$v[0]]['pid'] = $s[0];
                $type[$v[0]]['qaun'] = $s[2];
                $type[$v[0]]['concurrent'] = $s[3];
                $type[$v[0]]['run_rate'] = $s[1];
            } 
        }
       
        $info->type = $type;
        $this->assign("list",$list);
        $this->assign("info",$info);
        return view("mch_channel");
    }
    /**
     * Undocumented 代理商列表
     *
     * @return void
     */
    public function agent(){
        $list = Mch::where("is_agent",2)->field("id,agent_rate,mch,mch_name,balance,freeze,rukuan,status,chukuan,create_time")->paginate(10,false,['query' =>$this->request->param()]);
        $page = $list->render();
        $this->assign("list",$list);
        $this->assign("page",$page);
        return view("agent");
    }
    /**
     * Undocumented 商户收益
     *
     * @return void
     */
    public function mch_record(){
        
        $list = Db::name("mch_record")->alias('a')->order("a.id",'desc')->where(function($query){
            
            //商户号/商户名称
            $keyword = trim(input("keyword",''));
            if(!empty($keyword)){
                $query->where("b.mch",'like','%'.$keyword.'%')->whereOr("b.mch_name",'like','%'.$keyword.'%');
            }
            //类型
            $type = trim(input("type",''));
            if(!empty($type)){
                $query->where("a.type",$type);
            }
            //订单号
            $order_sn = trim(input("order",''));
            if(!empty($order_sn)){
                $query->where("a.order_sn",'%'.$order_sn.'%');
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:059',time()));
            if(!empty($start) && !empty($end)){
                $query->where("a.create_time",'between',[$start,$end]);
            }
        })->join("mm_mch b",'a.mch_id = b.mch')->field("a.*")->paginate(10,false,['query' =>$this->request->param()]);
        //导出
        if(!empty($this->request->param('mch_record_export')) && $this->request->param('mch_record_export') == 'export'){
            $this->mch_record_export($this->request->param());
        }
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("mch_record");
    }
    /**
     * Undocumented 代理商收益明细
     *
     * @return void
     */
    public function detail(){
       
        $list = Db::name("agent_record")->order("id",'desc')->where(function($query){
            $agent_id = input('id');
            if(!empty($agent_id)){
                $query->where("agen_id",$agent_id);
            }
            //类型
            $type = trim(input("type",''));
            if(!empty($type)){
                $query->where("type",$type);
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:059',time()));
            if(!empty($start) && !empty($end)){
                $query->where("create_time",'between',[$start,$end]);
            }
        })
        ->paginate(10,false,['query' =>$this->request->param()]);
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("agent_detail");
    }
}


?>