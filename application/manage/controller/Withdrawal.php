<?php

namespace app\manage\controller;

use app\model\Admin;
use app\model\Mch;
use think\Db;
use \app\model\Withdrawal as with;
class Withdrawal extends Base{
     /**
     * Undocumented 构造方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->assign("data",$this->request->param());
    }
    /**
     * Undocumented 出款记录
     *
     * @return void
     */
    public function index(){
 
        $list = with::order('a.id','desc')->alias('a')->where(function($query){
            $status = input("status",'');
            if(!empty($status)){
                $query->where("a.status",$status);
            }
            $keyword = input("keyword",'');
            if(!empty($keyword)){
                $query->where("a.mch_id",'like','%'.$keyword.'%');
            }
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:59',time()));
            if(!empty($start) && !empty($end)){
                $query->where("a.create_time",'between',[$start,$end]);
            }
        })->join('mm_bank_card b','a.bank_id = b.id')->field('a.*,b.card_number,b.account_name,b.bank_name,b.branch,b.location')
        ->paginate(10,false,['query' =>$this->request->param()]);
        //导出
        if(!empty($this->request->param('order_export')) && $this->request->param('order_export') == 'export'){
            $this->with_export($this->request->param());
        }
        $page = $list->render();
        $this->assign("list",$list);
        $this->assign("page",$page);
        return view("index");
    }
    /**
     * Undocumented 锁定/解除
     *
     * @return boolean
     */
    public function isLock(){
        $type = $this->request->param("type",'','strip_tags,trim');//2、锁定 1、解除
        $id = $this->request->param("id",'','strip_tags,trim');
        $info = with::where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        if($type == 2){
            if($info->status == 2){
                return $this->error("该笔出款已由".$info->lock_name."处理");
            }
            if(in_array($info->status,[3,4])){
                return $this->error("该笔订单已完成或者已取消");
            }
            $info->lock_name = $this->request->admin_auth['user_name'];
            $info->lock = $type;
            $info->status = 2;
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'锁定出款订单['.$info->with_out_no.']');
        }else{//解除
            if($info->lock_name != $this->request->admin_auth['user_name']){
                return $this->error("只能由账号：".$info->lock_name.'来解除');
            }
            $info->lock_name = '';//操作人
            $info->channel = '';//渠道
            $info->status = 1;//出款状态
            $info->lock = $type;//锁定状态
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],'解除出款订单['.$info->with_out_no.']');
        }
        $info->save();
        return $this->success("操作成功");
    }
    /**
     * Undocumented 编辑渠道
     *
     * @return void
     */
    public function chan_edit(){
        $name = $this->request->param("name",'','strip_tags,trim');
        $id = $this->request->param("id",'','strip_tags,trim');
        $info = with::where("id",$id)->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        if($info->lock == 2){
            if($info->lock_name != $this->request->admin_auth['user_name']){
                return $this->error("只能由账号：".$info->lock_name.'来编写');
            }
        }
        $info->channel = $name;
        $info->save();
        return $this->success("操作成功");
    }
    /**
     * Undocumented 出款/拒绝
     *
     * @return void
     */
    public function with_operation(){
        $data = $this->request->param();
       
        $info = with::where("id",$data['wid'])->find();
        if(!$info){
            return $this->error("数据不存在");
        }
        if(empty($info->lock_name)){
            return $this->error("请先锁定后再执行此操作");
        }
        if($info->lock_name != $this->request->admin_auth['user_name']){
            return $this->error("只能由账号：".$info->lock_name.'来操作');
        }
        if(in_array($info->status,[3,4])){
            return $this->error("该笔订单已完成或者已取消");
        }
        $result = with::withUpdate($info->id,$info->mch_id,$info->type,$info->total_fee,$data,$info->with_out_no);
        if($result === true){
            $name = $info->type == 1 ? '给商户号['.$info->mch_id.']' : "给代理商[".Mch::getAgent($info->mch_id)."]";
            if($data['type'] == 'ru'){//出款
                $action = $name.'出款'.$info->total_fee.'元成功';
            }else{//拒绝出款
                $action = $name.'拒绝出款,拒绝金额:'.$info->total_fee.'元';
            }
            getLog($this->request->admin_auth['auth_admin_id'],$this->request->admin_auth['role_id'],$action);
            return $this->success("操作成功");
        }else{
            return $this->error($result);
        }
    }

}