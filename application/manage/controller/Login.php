<?php

namespace app\manage\controller;

use think\captcha\Captcha;
use \app\model\Admin;
class Login extends Base
{
    /**
     * 登录
     */
    public function index(){
       
        $admin_auth = Session("auth_admin");
        if($admin_auth){
           return $this->redirect('/manage/home');
        }
        return view("home/login");
    }
    /**
     * 登录验证
     *
     * @return void
     */
    public function checkLogin(){
        
        $user_name = $this->request->param("user_name",'','strip_tags,trim');
        $password = $this->request->param("password",'','strip_tags,trim');
        $googl_code = $this->request->param("googl_code",'','strip_tags,trim');
        //$code = $this->request->param("code",'','strip_tags,trim');
        if(empty($user_name) || empty($password) || empty($googl_code)){
            return $this->error("缺少必填参数");
        }
        // if(!captcha_check($code)){
        //     return $this->error("验证码错误");
        // }
        $admin = new Admin();
        $res = $admin->auth_admin($user_name,$password,$googl_code);
        if($res['status'] == 1){
            return $this->error($res['msg']);
        }
        return $this->success($res['msg'],'/manage/home');
        
    }
    /**
     * 验证码
     *
     * @return void
     */
    public function verify(){
        $config =    [
            'imageH'      =>    30,     // 高度
            'codeSet'     =>    '0123456789',     // 设置验证码字符为纯数字
            'imageW'      =>    120,     // 宽度
            'fontSize'    =>    16,     // 验证码字体大小
            'length'      =>    4,   // 验证码位数
            'useNoise'    =>    true, // 关闭验证码杂点
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }
    /**
     * 退出登录
     */
    public function logout(){
       
        Session('auth_admin',null);
        return $this->error('退出成功','/manage/login');
    }
}