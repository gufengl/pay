<?php
namespace app\web\controller;

use app\model\Mch;
use think\Db;
use app\model\Order;

class Index extends Base
{
    /**
     * Undocumented 首页
     *
     * @return void
     */
    public function index()
    {

        return view("home/index");
      
   
    }
    /**
     * Undocumented 欢迎页
     *
     * @return void
     */
    public function welcome(){

        $info = Mch::where("id",$this->request->auth_mch['mch_id'])->find();
        $list = array();
        $start = date("Y-m-d 00:00:00",time());
        $end = date("Y-m-d 23:59:59",time());
        if($this->request->auth_mch['is_agent'] == 2){
            $mch = Mch::where("agent_id",$this->request->auth_mch['mch_id'])->column("mch");
            $list['agent_amount'] = Order::where('create_time','between',[$start,$end])->where("pay_status",2)->where("mch_id",'in',$mch)->sum('agent_amount');
        }else{
            $list = Order::where('create_time','between',[$start,$end])->where("mch_id",$this->request->auth_mch['mch'])
            ->field("sum(if(pay_status=2,settle,0)) as settle,sum(amount) as amount,count(id) as max_count,sum(if(pay_status=2,actual_amount,0)) as actual_amount,sum(if(pay_status=2,1,0)) as yes_num,sum(if(pay_status!=2,1,0)) as no_num,sum(if(pay_status!= 2,amount,0)) as no_amount")->select()->toArray();
        }
        $this->assign("info",$info);
        $this->assign("list",$list);
        return view("home/welcome");
    }

    
}
