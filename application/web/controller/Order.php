<?php

namespace app\web\controller;

use app\model\Order as Orders;
use app\model\Mch;
use think\Db;

class Order extends Base
{
     /**
     * Undocumented 构造方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->assign("data",$this->request->param());
        $this->assign("payment",config("custom.PAYMENT_TYPE"));
    }
    /**
     * Undocumented 
     *
     * @return void
     */
    public function index(){
        $list = Orders::order("id",'desc')->where(function($query){
            //代理商
            if($this->request->auth_mch['is_agent'] == 2){
                $mch = Mch::where("agent_id",$this->request->auth_mch['mch_id'])->column("mch");
                if($mch){
                    $query->where("mch_id",'in',$mch);
                }
            }else{
                $query->where("mch_id",$this->request->auth_mch['mch']);
            }
            //支付状态
            $status = input("status");
            if(!empty($status)){
                $query->where("pay_status",$status);
            }
            //通知状态
            $notice = input("notice");
            if(!empty($notice)){
                $query->where("notice",$notice);
            }
            //商户单号/商户号
            $keyword = input("keyword");
            if(!empty($keyword)){
                $query->where("out_trade_no|mch_id",'like','%'.$keyword.'%');
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d H:i:s',time()));
            if(!empty($start) && !empty($end)){
                $query->where("create_time",'between',[$start,$end]);
            }
        })
        ->field('id,mch_id,out_trade_no,amount,actual_amount,agent_amount,run_rate,total_fee,settle,create_time,pay_time,pay_status,notice,pay_code')
        ->paginate(10,false,['query' =>$this->request->param()]);
         //导出
         if(!empty($this->request->param('order_export')) && $this->request->param('order_export') == 'export'){

            getMchLog($this->request->auth_mch['mch'],'下载订单报表',$this->request->auth_mch['is_agent']);
           
            $this->order_export($this->request->param(),$this->request->auth_mch);
        }
        $page = $list->render();
        $this->assign("list",$list);
        $this->assign("page",$page);
        return view("index");
    }
    /**
     * Undocumented 商户资金变更
     *
     * @return void
     */
    public function waterRecord(){
        $list = Db::name("mch_record")->alias('a')->where("a.mch_id",$this->request->auth_mch['mch'])->where(function($query){
            //订单号
            $keyword = trim(input("keyword",''));
            if(!empty($keyword)){
                $query->where("order_sn",'like','%'.$keyword.'%');
            }
            $type = trim(input("type",''));
            if(!empty($type)){
                $query->where("type",$type);
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:059',time()));
            if(!empty($start) && !empty($end)){
                $query->where("create_time",'between',[$start,$end]);
            }
        })->paginate(10,false,['query' =>$this->request->param()]);
        
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        
        return view("water_record");

    }
    /**
     * Undocumented 代理资金变更记录
     *
     * @return void
     */
    public function waterAgentRecord(){
        $list = Db::name("agent_record")->order("id",'desc')->where(function($query){
            //类型
            $type = trim(input("type",''));
            if(!empty($type)){
                $query->where("type",$type);
            }
            //创建时间区间
            $start = input("start",date('Y-m-d 00:00:00',time()));
            $end = input("end",date('Y-m-d 23:59:059',time()));
            
            $query->where("create_time",'between',[$start,$end]);
            
        })
        ->paginate(10,false,['query' =>$this->request->param()]);
        $page = $list->render();
        $this->assign("page",$page);
        $this->assign("list",$list);
        return view("agent_water_record");
    }
}