<?php
namespace app\web\controller;

use think\Controller;
use app\model\Order;
use app\model\Mch;
use app\model\Channel;

class Base extends Controller
{
    /**
     * Undocumented 订单导出
     *
     * @return void
     */
    public function order_export($data,$type){
        
        $list = Order::order("id",'desc')->where(function($query) use($data,$type){
            //代理商
            if($type['is_agent'] == 2){
                $mch = Mch::where("agent_id",$type['mch_id'])->column("mch");
                if($mch){
                    $query->where("mch_id",'in',$mch);
                }
            }else{
                $query->where("mch_id",$type['mch']);
            }
            //支付状态
            if(!empty($data['status'])){
                $query->where("pay_status",$data['status']);
            }
            //通知状态
            if(!empty($data['notice'])){
                $query->where("notice",$data['notice']);
            }
            //商户单号/商户号
            if(!empty($data['keyword'])){
                $query->where("out_trade_no|mch_id",'like','%'.$data['keyword'].'%');
            }
            //创建时间区间
            $start = !empty($data['start']) ? $data['start'] : date('Y-m-d 00:00:00',time());
            $end = !empty($data['end']) ? $data['end'] : date('Y-m-d H:i:s',time());
            $query->where("create_time",'between',[$start,$end]);
            
        })
        ->field("id,mch_id,out_trade_no,amount,actual_amount,run_rate,total_fee,settle,pay_status,IF(notice=1,'未通知','已通知') AS notice,pay_code as payment,pay_time,create_time")
        ->select()->toArray();
       
        $title = [
            "ID",
            "商户号",
            "订单号",
            "下单金额",
            "实际支付",
            "费率",
            "手续费",
            "结算金额",
            "支付状态",
            "通知状态",
            "支付类型",
            "支付时间",
            "下单时间"
        ];
        
        return $this->export($list,'订单导出',$title);
    }
    private function export($list,$name, $title){
       
        ini_set('max_execution_time', 100);// 设置PHP超时时间
        ini_set('memory_limit', '500M');// 设置PHP临时允许内存大小
         //路径
         $fileName = $name. date('Ymd_His') . '.csv';
         $filePath = '../' . $fileName;
         $index = 0;
         $fp = fopen($filePath, 'w'); //生成临时文件
         chmod($filePath, 0777);//修改可执行权限
        fputcsv($fp, $title);
        foreach ($list as  &$item) {
            if(!empty($item['pay_status'])){
                switch ($item['pay_status']) {
                    case '1':
                        $item['pay_status'] = '未支付';
                        break;
                    case '2':
                        $item['pay_status'] = '已支付';
                        break;
                    
                    default:
                        $item['pay_status'] = '已关闭';
                        break;
                }
            }
            if(!empty($item['payment'])){
                $item['payment'] = Channel::paymentType($item['payment']);
            }
            foreach ($item as $k => $v) {
                $item[$k] = $v . "\t";
                //每次写入1000条数据清除内存
                if ($index == 10000) { 
                    $index = 0;
                    ob_flush();//清除内存
                    flush();
                }
                $index++;
            }
            fputcsv($fp, $item);
        }
        ob_flush();
        fclose($fp);  //关闭句柄
        header("Cache-Control: max-age=0");
        header("Content-type:application/vnd.ms-excel;charset=UTF-8");
        header("Content-Description: File Transfer");
        header('Content-disposition: attachment; filename=' . basename($fileName));
        header("Content-Type: text/csv");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filePath));
        @readfile($filePath);//输出文件;
        unlink($filePath); //删除压缩包临时文件
    }
}