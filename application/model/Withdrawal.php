<?php

namespace app\model;

use think\Model;
use think\DB;
use app\model\Admin;
use app\model\Mch;

class Withdrawal extends Model{
    /**
     * Undocumented 创建提现申请
     *
     * @param [type] $mch         商户号
     * @param [type] $bank_id     银行卡id
     * @param [type] $total_fee   申请金额
     * @param [type] $w_fee       提现手续费
     * @param [type] $type        角色 1、商户 2代理商
     * @return void
     */
    public static function with_insert($mch,$bank_id,$total_fee,$w_fee,$type){
        $with_out_no = getOrderId();
        Db::startTrans();
        try { 
            Withdrawal::insert(array(
                "mch_id"       => $mch,//商户号
                "with_out_no"  => $with_out_no,
                "bank_id"      => $bank_id,//银行卡id
                "poundage"     => $w_fee, //手续费
                "total_fee"    => $total_fee,//申请金额
                "actual_fee"   => $total_fee - $w_fee, //实际到账金额
                "type"         => $type,//角色
                "create_time"  => date("Y-m-d H:i:s",time())
            ));
            Mch::where("mch",$mch)->setDec("balance",$total_fee);
            if($type == 2){
                mchAgentRecord($mch,'申请提现',$total_fee,7,$with_out_no);
            }else{
                mchRecord($mch,'申请提现',$total_fee,7,$with_out_no);
            }
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }
        return true;
    }
    /**
     * Undocumented 更新提现记录
     *
     * @param [type] $id
     * @param [type] $mch        商户号
     * @param [type] $type       1、代理商 2、商户
     * @param [type] $total_fee  申请金额
     * @param [type] $data
     * @param [type] $with_out_no订单号
     * @return void
     */
    public static function withUpdate($id,$mch,$type,$total_fee,$data,$with_out_no){
        Db::startTrans();
        try {  
            if($data['type'] == 'ru'){//出款
                Withdrawal::where("id",$id)->update(array("remark"=>$data['text'],"status"=>3));
            }else{//拒绝出款
                Withdrawal::where("id",$id)->update(array("status"=>4,'channel'=>'','remark'=> trim($data['remark'])));
                //退回商户余额
                if($type == 2){
                    Admin::where("id",$mch)->setInc("balance",$total_fee);
                    mchAgentRecord($mch,'提现申请失败退回',$total_fee,8,$with_out_no);
                }else{
                    Mch::where("mch",$mch)->setInc("balance",$total_fee);
                    mchRecord($mch,'提现申请失败退回',$total_fee,8,$with_out_no);
                }
            }
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }
        return true;
    }
    /**
     * Undocumented 添加/编辑银行卡
     *
     * @return void
     */
    public static function backUpdate($data,$mch){
        if(!empty($data['p_id'])){
            //更新
            Db::startTrans();
            try {
                Db::name("bank_card")->where("id",$data['p_id'])->update(array(
                    "card_number" => trim($data['card_number']),
                    "account_name" => trim($data['account_name']),
                    "bank_name" => trim($data['bank_name']),
                    "branch" => trim($data['branch']),
                    "location" => $data['location'],
                ));
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return $e->getMessage();
            }
            return true;
        }else{
            //添加
            Db::startTrans();
            try {
                Db::name("bank_card")->insert(array(
                    "mch_id" => $mch,
                    "card_number" => trim($data['card_number']),
                    "account_name" => trim($data['account_name']),
                    "bank_name" => trim($data['bank_name']),
                    "branch" => trim($data['branch']),
                    "location" => $data['location'],
                    "create_time" => date("Y-m-d H:i:s",time()),
                ));
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return $e->getMessage();
            }
            return true;
        }
    }








}
?>