<?php

namespace app\model;
use think\Model;
use think\Db;

class Channel extends Model{


    public static function init()
    {
        self::event('after_update', function ($channel) {
            Db::name("payment")->where("channel_id",$channel->id)->update(array("status"=>$channel->status));
            $list = Db::name("payment")->select();
            get_list_insert('payment',$list);
            self::mch_conf();
        });
    }
    /**
     * Undocumented 添加/更新渠道
     *
     * @param [type] $data
     * @return void
     */
    public static function channel_creat($data){
        $res = array(
            "title" =>trim($data['name']),//渠道名称
            "code" =>trim($data['channel_class']),//渠道类名
            "status" => $data['status'],//状态 1、开启 2、关闭
            "white_ip" =>trim($data['white_ip']),//ip
            "create_time" =>date("Y-m-d H:i:s",time())//创建时间
        );
        Db::startTrans();
        try {
            if(isset($data['id'])){//更新
                unset($data['create_time']);
                Channel::where("id",$data['id'])->update($res);
            }else{//添加
                 Channel::insertGetId($res);
            }
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }
        $list = Channel::field("id,title,code,status,white_ip")->select();
        get_list_insert('channel',$list);
        self::mch_conf();
        return true;
    }

    public static function mch_conf(){
        $redis = new \Predis\Client();
        $redis->del("channel_type");
        $list = Channel::select()->toArray();
        foreach($list as $k => $v){//遍历所有渠道
            //查询渠道配置的支付类型
            $lists = Db::name("payment")->where("channel_id",$v['id'])->select();
            if(!empty($lists)){
                $list[$k]['list'] = $lists;
                $list[$k]['count'] = count($lists);//统计总数
            }else{
                unset($list[$k]);//删除空的数据
            }
        }
        $redis->set('channel_type',json_encode($list));
    }
    /**
     * Undocumented 添加/编辑支付类型
     *
     * @param [type] $data
     * @return void
     */
    public static function payment($data){
        $res = array(
            "channel_id"    => $data['channel_id'],            //渠道id
            "code"          => trim($data['channel_code']),    //渠道编码
            "pay_type"      =>$data['payment'],                //支付类型
            "cost"          => trim($data['cost_rate']),       //成本
            "qualified"     => trim($data['qualified']),       //当日前额
            "min_amount"    => trim($data['min_amount']),      //最低
            "max_amount"    => trim($data['max_amount']),      //最高
            "fixed_amount"  => trim($data['fixed_amount']),    //固定金额
            "double_amount" => trim($data['double_amount']),   //倍数
            "exclude_tail"  => trim($data['exclude_tail']),    //排除尾数
            "create_time"   => date("Y-m-d H:i:s",time())      //创建时间
        );
        Db::startTrans();
        try {
            if(!empty($data['pay_id'])){//更新
                Db::name("payment")->where("id",$data['pay_id'])->update($res);
            }else{//新增
                Db::name("payment")->insertGetId($res);
            }
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }
        $list = Db::name("payment")->select();
        get_list_insert('payment',$list);
        self::mch_conf();
        return true;
    } 
    /**
     * Undocumented 获取渠道名称
     *
     * @param [type] $channel
     * @return void
     */
    public static function channel_name($channel){

        $info = get_one("channel",$channel);
        if(!empty($info)){
            return $info['title'];
        }
        $list = Channel::select();
        get_list_insert('channel',$list);
        return Channel::where("id",$channel)->value("title");
    }
    /**
     * Undocumented 获取渠道信息
     *
     * @param [type] $channel
     * @return void
     */
    public static function get_channel($channel){

        $info = get_one("channel",$channel);
        if(!empty($info)){
            return $info;
        }
        $list = Channel::select();
        get_list_insert('channel',$list);
        return Channel::where("id",$channel)->find();
    }
   
    /**
     * Undocumented 根据支付类型id获取渠道名称
     *
     * @param [type] $id
     * @return void
     */
    public static function payment_name($channel_id){
       
        $channel = get_one("channel",$channel_id);
        if($channel){
            return $channel['title'];
        }else{
            $list = Channel::select();
            get_list_insert('channel',$list);
            return Channel::where("id",$channel_id)->value('title');
        }
        
    }
    /**
     * Undocumented 获取支付类型名称
     *
     * @return void
     */
    public static function paymentType($type){
        
        $payment = config("custom.PAYMENT_TYPE");
        
        return $payment[$type];
    }
   
    /**
     * Undocumented 获取支付类型信息
     *
     * @param [type] $payment_id
     * @return void
     */
    public static function get_payment($payment_id){
        $p = get_one("payment",$payment_id);
        if(empty($p)){
            $list = Db::name("payment")->select();
            get_list_insert('payment',$list);
            $p = Db::name("payment")->where("id",$payment_id)->find();
            if($p){
                //重新导入redis
                get_one_insert('payment',$p['id'],$p);
            }else{
                $p = array();
            }
        }
        return $p;
    }










}
?>