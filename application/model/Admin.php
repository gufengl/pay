<?php

namespace app\model;

use think\Model;
use think\Db;

class Admin extends Model{
    /**
     * 登录检测
     *
     * @param [type] $username 账号
     * @param [type] $password 密码
     * @return void
     */
    public function auth_admin($username,$password,$code = ''){
        $info = Admin::where('user_name',$username)->find();
        if(!$info){
            return ['status'=>1,'msg'=>"账号不存在"];
        }
        if($info->type == 2){
            return ['status'=>1,'msg'=>"账号已禁用,请联系管理员"];
        }
        $Aes = new \Aes();
        $pwd = $Aes->decrypt($info->password);//解密
        if($pwd != $password){
            return ['status'=>1,'msg'=>"密码错误"];
        }
        //谷歌验证
        if($info->googl_secret){
            if(!Googl_validation($info->googl_secret,$code)){
                //return ['status'=>1,'msg'=>"谷歌验证码错误"];
            }
        }
        $info->key = md5key(6);//随机数
        $info->save();
        //保存登录信息
        Session("auth_admin",['auth_admin_id'=>$info->id,'user_name'=>$info->user_name,'key'=>$info->key,'role_id'=>$info->role_id]);
        get_one_insert('auth_admin',$info->id,$info);
        getLog($info->id,$info->role_id,'账号['.$info->user_name.']登录后台');
        return ['status'=>2,'msg'=>"登录成功"];
    }
    /**
     * Undocumented 账号信息
     *
     * @param [type] $id
     * @return void
     */
    public static function get_auth($id){

        $info = get_one("auth_admin",$id);
        if($info){
            return $info;
        }
        return Admin::where("id",$id)->find()->toArray();
    }
    
    /**
     * Undocumented 获取管理账号昵称
     *
     * @param [type] $id
     * @param [type] $type
     * @return void
     */
    public static function admin_name($id,$type){
        $info = get_one("auth_admin",$id);
        if($info){
            if($type == 1){
                return $info['user_name'];
            }
            return $info['nickname'];
        }
        if($type == 1){
            return Admin::where("id",$id)->value("user_name");
        }
        return Admin::where("id",$id)->value("nickname");
    } 
    /**
     * Undocumented 添加账号
     *
     * @param [type] $data
     * @return void
     */
    public static function adminAdd($data){
        //print_r($data);exit;
        $Aes = new \Aes();
        Db::startTrans();
        try {
            if(!empty($data['role_id'])){//更新
                if(!empty(trim($data['pwd']))){//密码不为空
                    Admin::where("id",$data['role_id'])->update(array(
                        "password" =>$Aes->encrypt(trim($data['pwd'])),
                        "nickname" => trim($data['nickname']),
                        "user_name" => trim($data['name']),
                        "role_id" =>$data['pid']
                    ));
                }else{//为空就不更新密码
                    Admin::where("id",$data['role_id'])->update(array(
                        "user_name" => trim($data['name']),
                        "nickname" => trim($data['nickname']),
                        "role_id" =>$data['pid']
                    ));
                }
            }else{//新增账号
                Admin::insertGetId(array(
                    "user_name" => trim($data['name']),
                    "nickname" => trim($data['nickname']),
                    "password" =>$Aes->encrypt(trim($data['pwd'])),
                    "role_id" =>$data['pid'],
                    "create_time"=> date('Y-m-d H:i:s',time())
                ));
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();

            return $e->getMessage();
        }
        return true;
    }
   
}










?>