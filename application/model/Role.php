<?php

namespace app\model;

use think\Model;
use think\Cache;
class Role extends Model{

    protected $table = 'mm_role';
    
    /**
     * Undocumented 角色
     *
     * @param [type] $id
     * @return void
     */
    public static function roleType($id){
        switch($id){
            case 2:
                $text =  "其 他 ";
                break;
            case 3:
                $text =  "运&nbsp;营";
                break;
            case 4:
                $text =  "客&nbsp;服";
                break;
            case 5:
                $text =  "财&nbsp;务";
                break;
            default:  
                $text =  "超级管理员";
        }
      return $text;
    }
    /**
     * Undocumented 获取下拉角色列表
     *
     * @param [type] $pid
     * @return void
     */
    public static function selectRole($pid = 0){

        $list = Role::cache(true)->select();
        $html = "<select id='select' class='form-control m-b' name='account'><option value='0'>请选择角色</option>";
        foreach($list as $k => $v){
            $selected = "";
            if($v['id'] == $pid){
                $selected = 'selected';
            }
            $html .= '<option '.$selected.' value='.$v['id'].'>'.$v['role_name'].'</option>';
        }
        $html .= "</select>";
        return $html;
    }

}




    





?>