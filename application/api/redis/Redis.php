<?php

namespace app\api\redis;
use think\Db;
use think\queue\Job;
    
class Redis{
    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data)
      {
          // 有些消息在到达消费者时,可能已经不再需要执行了
          $isJobStillNeedToBeDone = $this->checkDatabaseToSeeIfJobNeedToBeDone($data);
          if(!$isJobStillNeedToBeDone){
              $job->delete();
              return;
          }
        
          $isJobDone = $this->doHelloJob($data);
        
          if ($isJobDone) {
              //执行成功， 记得删除任务
              $job->delete();
              print("<info>执行成功"."</info>\n");
          }else{//执行失败
            
              //查看任务执行了几次
              if ($job->attempts() > 10) {
                  //通过这个方法可以检查这个任务已经重试了几次了
                  print("<warn>执行失败10后删除任务"."</warn>\n");
                  //删除任务
  				    $job->delete();
                  
                  // 也可以重新发布这个任务
                  //print("<info>Hello Job will be availabe again after 2s."."</info>\n");
                  //$job->release(2); //$delay为延迟时间，表示该任务延迟2秒后再执行
              }
          }
      }
      
      /**
       * 有些消息在到达消费者时,可能已经不再需要执行了
       * @param array|mixed    $data     发布任务时自定义的数据
       * @return boolean                 任务执行的结果
       */
      private function checkDatabaseToSeeIfJobNeedToBeDone($data){
          return true;
      }

      /**
       * Undocumented 这里编写需要处理的业务逻辑php think queue:listen --queue MQ:order
       *
       * @param [type] $data
       * @return void
       */
      private function doHelloJob($data) 
      {
          Db::name("test")->insert(['data'=>json_encode($data)]);
          print("<info>Hello Job Started. job Data is: ".var_export($data,true)."</info> \n");
          print("<info>Hello Job is Fired at " . date('Y-m-d H:i:s') ."</info> \n");
          print("<info>Hello Job is Done!"."</info> \n");
          
          return true;
      }
}
















?>