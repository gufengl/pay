<?php

namespace app\api\controller;

use app\model\Mch;

use think\Db;

class Test extends Base
{
    public  static $mch_id = '1000950100';//商户号

    public  static $KEY = 'b037ac59ff5fe7a8e1abc8890204618d';//商户秘钥

    /**
     * Undocumented 测试
     *
     * @return void
     */
    public function index(){
        
        $data = $this->request->param();
        $native = [
            "mch_id" => self::$mch_id,//商户号
            "amount" => (int)$data['amount']*100,//下单金额 分
            "out_trade_no" => time(),//订单号
            "goods" => 'wlf',//商品描述
            "service_type" => $data['channel'],//支付类型
            "callback_url" =>APP_URL.'/pay/test/notify',//异步回调
            "return_url" => APP_URL.'/pay/test/callback',//同步跳转
        ];
        $native['sign'] = get_sign($native,self::$KEY);
        $result = json_decode(curl_post(APP_URL.'/pay/gateway',$native));
        if($result->code == 1){
            Header("Location:".$result->data->link_url);
        }else{
            exit($result->msg);
        }
    }
    /**
     * Undocumented 异步回调
     *
     * @return void
     */
    public function notify(){
        $data = $this->request->param();
        //写入日志
        get_log($data,'Yunlai');
        echo 'success';
    }
    /**
     * Undocumented 同步跳转
     *
     * @return void
     */
    public function callback(){
        exit('支付成功');
    }

}