<?php
namespace app\api\controller;

use think\Controller;

class Base extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //验证黑名单
        if(get_is_ip() === false){
           
            echo json_encode(['code'=>0,'msg'=>'该IP已列入黑名单'],JSON_UNESCAPED_UNICODE);exit;
        }
        $redis = new \Predis\Client();
        if($redis->get('web') == 1){
            echo json_encode(['code'=>0,'msg'=>'网站维护中，请等待。。。。。。。'],JSON_UNESCAPED_UNICODE);exit;
        }
        //限制接口并发
        $concurrent = config("custom.CONCURRENT");
        if($concurrent['key'] && $concurrent['time'] && $concurrent['max_request']){
            $result = redis_concurrent($concurrent['key'],$concurrent['time'],$concurrent['max_request']);
            if($result){
                echo json_encode(['code'=>0,'msg'=>$result],JSON_UNESCAPED_UNICODE);exit;
            } 
        }
    }
    public function jsonReturn($data){

        if(is_array($data)){
            return json([
                "code" => 1,
                "data" => $data
            ]);
        }
        return json([
            "code" => 0,
            "msg" => $data
        ]);
    }


}