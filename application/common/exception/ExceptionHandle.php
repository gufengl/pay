<?php

namespace app\common\exception;

use Exception;
use \think\exception\Handle;

class ExceptionHandle extends Handle
{

    public function render(Exception $e)
    {
        //ajax、post和手机请求返回json
        if(request()->isAjax() || request()->isPost()){
            return json([
                'code' => '0',
                'msg'  =>$e->getMessage()
            ]);
        }
        // 其他错误交给系统处理
        return parent::render($e);
    }
    
}
