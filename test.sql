/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.26 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `mm_admin` */

DROP TABLE IF EXISTS `mm_admin`;

CREATE TABLE `mm_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL COMMENT '账号',
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `role_id` tinyint(1) unsigned NOT NULL COMMENT '角色id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 2禁用 1启用',
  `googl_secret` varchar(20) DEFAULT NULL COMMENT '谷歌秘钥',
  `key` int(7) unsigned DEFAULT NULL COMMENT '随机数',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `账号` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `mm_admin` */

insert  into `mm_admin`(`id`,`user_name`,`nickname`,`password`,`role_id`,`type`,`googl_secret`,`key`,`update_time`,`create_time`) values (1,'admin','超级管理员','4yMg1zZWX1i4z+MHzxYNpw==',1,1,'52SH6XEV2ZY2DH4A',4994075,'2020-01-14 13:38:13','2019-12-31 14:12:06'),(2,'caiwu001','财务啊发','4yMg1zZWX1i4z+MHzxYNpw==',5,1,'AVDULIK7MISEZNG2',3046480,'2020-01-15 13:38:21','2020-01-02 11:12:00');

/*Table structure for table `mm_agent_channel` */

DROP TABLE IF EXISTS `mm_agent_channel`;

CREATE TABLE `mm_agent_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL COMMENT '代理商户号',
  `payment_id` int(11) DEFAULT NULL COMMENT '支付类型id',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_agent_channel` */

insert  into `mm_agent_channel`(`id`,`agent_id`,`payment_id`,`channel_id`) values (1,1000886100,1,4);

/*Table structure for table `mm_agent_record` */

DROP TABLE IF EXISTS `mm_agent_record`;

CREATE TABLE `mm_agent_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `agen_id` int(11) unsigned NOT NULL COMMENT '代理商id',
  `balance` decimal(10,3) DEFAULT NULL COMMENT '变更金额',
  `total_balance` decimal(10,3) DEFAULT NULL COMMENT '变更后的余额',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `type` tinyint(5) unsigned DEFAULT '1' COMMENT '1、增加 2、减少 3、冻结 4、解冻 5、支付',
  `order_sn` varchar(32) DEFAULT NULL COMMENT '订单号',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mm_agent_record` */

/*Table structure for table `mm_bank_card` */

DROP TABLE IF EXISTS `mm_bank_card`;

CREATE TABLE `mm_bank_card` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` int(11) DEFAULT NULL COMMENT '商户号',
  `card_number` varchar(25) DEFAULT NULL COMMENT '银行卡号',
  `account_name` varchar(30) DEFAULT NULL COMMENT '开户名称',
  `bank_name` varchar(100) DEFAULT NULL COMMENT '所属银行',
  `branch` varchar(100) DEFAULT NULL COMMENT '所在支行',
  `location` varchar(200) DEFAULT NULL COMMENT '开卡城市',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mch` (`mch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_bank_card` */

insert  into `mm_bank_card`(`id`,`mch_id`,`card_number`,`account_name`,`bank_name`,`branch`,`location`,`create_time`,`update_time`) values (1,1000950100,'60302302620263606','张三','中国银行','龙岗支行','湖北省 - 武汉市 - 汉南区','2020-01-08 15:38:35','2020-01-08 15:38:35');

/*Table structure for table `mm_channel` */

DROP TABLE IF EXISTS `mm_channel`;

CREATE TABLE `mm_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL COMMENT '渠道名称',
  `code` varchar(30) DEFAULT NULL COMMENT '渠道类名',
  `status` tinyint(1) DEFAULT '1' COMMENT '1、开启 2、关闭',
  `white_ip` text COMMENT '白名单',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `mm_channel` */

insert  into `mm_channel`(`id`,`title`,`code`,`status`,`white_ip`,`create_time`,`update_time`) values (4,'云来支付','Yunlai',1,'127.0.0.1','2019-12-29 13:31:18','2020-01-07 17:45:03');

/*Table structure for table `mm_log` */

DROP TABLE IF EXISTS `mm_log`;

CREATE TABLE `mm_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) unsigned NOT NULL COMMENT '操作者id',
  `action` varchar(200) DEFAULT NULL COMMENT '操作内容',
  `type` tinyint(1) DEFAULT '1' COMMENT '角色',
  `ip` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

/*Data for the table `mm_log` */

insert  into `mm_log`(`id`,`mid`,`action`,`type`,`ip`,`create_time`) values (1,1,'账号[admin]登录后台',1,'127.0.0.1','2019-12-31 14:35:41'),(2,1,'创建商户[1000950100]成功',1,'127.0.0.1','2019-12-31 14:36:40'),(3,1,'创建商户[1000886100]成功',1,'127.0.0.1','2019-12-31 14:39:37'),(4,1,'创建商户[1000749700]成功',1,'127.0.0.1','2019-12-31 14:41:45'),(5,1,'创建商户[1000299100]成功',1,'127.0.0.1','2019-12-31 14:43:17'),(6,1,'创建商户[1000265700]成功',1,'127.0.0.1','2019-12-31 14:44:00'),(7,1,'账号[admin]登录后台',1,'127.0.0.1','2019-12-31 14:50:34'),(8,1,'修改商户号[1000886100]商户状态为:禁用',1,'127.0.0.1','2019-12-31 15:25:14'),(9,1,'修改商户号[1000886100]商户入金状态为:禁用',1,'127.0.0.1','2019-12-31 15:25:15'),(10,1,'修改商户号[1000950100]商户状态为:禁用',1,'127.0.0.1','2019-12-31 15:25:18'),(11,1,'修改商户号[1000950100]商户入金状态为:禁用',1,'127.0.0.1','2019-12-31 15:25:18'),(12,1,'创建支付类型[云来支付-支付宝扫码]的渠道',1,'127.0.0.1','2019-12-31 15:27:26'),(13,1,'修改商户号[1000950100]商户入金状态为:开启',1,'127.0.0.1','2019-12-31 15:33:48'),(14,1,'更新支付类型ID[1]信息',1,'127.0.0.1','2019-12-31 16:35:41'),(15,1,'账号[admin]登录后台',1,'127.0.0.1','2019-12-31 17:46:51'),(16,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-01 10:52:14'),(17,1,'创建商户[1000486100]成功',1,'127.0.0.1','2020-01-01 10:55:56'),(18,1,'创建商户[1000688500]成功',1,'127.0.0.1','2020-01-01 10:56:19'),(19,1,'创建商户[1000766100]成功',1,'127.0.0.1','2020-01-01 11:06:11'),(20,1,'创建商户[1000633800]成功',1,'127.0.0.1','2020-01-01 11:12:01'),(21,1,'创建商户[1000608700]成功',1,'127.0.0.1','2020-01-01 11:12:04'),(22,1,'创建商户[1000299900]成功',1,'127.0.0.1','2020-01-01 11:12:51'),(23,1,'修改商户[1000886100]信息',1,'127.0.0.1','2020-01-01 12:00:00'),(24,1,'创建商户[1000119700]成功',1,'127.0.0.1','2020-01-01 12:13:36'),(25,1,'创建商户[1000849900]成功',1,'127.0.0.1','2020-01-01 12:14:16'),(26,1,'创建商户[1000790400]成功',1,'127.0.0.1','2020-01-01 12:15:26'),(27,1,'修改商户号[1000790400]商户状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:12'),(28,1,'修改商户号[1000790400]商户状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:14'),(29,1,'修改商户号[1000790400]商户入金状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:15'),(30,1,'修改商户号[1000119700]商户状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:22'),(31,1,'修改商户号[1000119700]商户入金状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:23'),(32,1,'修改商户号[1000119700]商户状态为:禁用',1,'127.0.0.1','2020-01-01 12:16:24'),(33,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-01 17:30:04'),(34,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:41:31'),(35,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:41:46'),(36,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:42:14'),(37,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:43:37'),(38,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:44:03'),(39,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:44:13'),(40,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:51:43'),(41,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:52:01'),(42,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:52:18'),(43,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:52:54'),(44,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 10:57:53'),(45,1,'创建财务账号：[caiwu001]',1,'127.0.0.1','2020-01-02 11:12:00'),(46,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-02 11:13:18'),(47,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 11:14:36'),(48,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 13:44:20'),(49,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-02 14:23:14'),(50,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 15:05:25'),(51,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-02 17:54:19'),(52,1,'修改商户号[1000950100]商户入金状态为:禁用',1,'127.0.0.1','2020-01-02 18:46:19'),(53,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-03 13:53:49'),(54,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-03 15:58:50'),(55,1,'修改商户号[1000950100]商户入金状态为:开启',1,'127.0.0.1','2020-01-03 16:01:33'),(56,1,'更新支付类型ID[1]信息',1,'127.0.0.1','2020-01-03 16:01:59'),(57,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-04 13:22:03'),(58,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-04 19:36:39'),(59,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-05 19:11:41'),(60,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-05 19:14:20'),(61,2,'给商户[1000950100]强制入账96.700元,订单号:[H2019123115334984078952]',5,'127.0.0.1','2020-01-05 19:17:30'),(62,2,'给商户[1000950100]强制入账483.500元,订单号:[H2020010316020687382335]',5,'127.0.0.1','2020-01-05 19:18:53'),(63,2,'给商户[1000950100]强制入账96.700元,订单号:[H2019123115334984078952]',5,'127.0.0.1','2020-01-05 19:47:51'),(64,2,'给商户[1000950100]补发通知,订单号:[H2020010316020687382335]',5,'127.0.0.1','2020-01-05 19:47:58'),(65,2,'给商户[1000950100]补发通知,订单号:[H2019123115334984078952]',5,'127.0.0.1','2020-01-05 19:48:03'),(66,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-06 10:30:17'),(67,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-06 10:33:09'),(68,1,'更新渠道[云来支付]状态为:关闭',1,'127.0.0.1','2020-01-06 10:33:23'),(69,1,'更新渠道[云来支付]状态为:开启',1,'127.0.0.1','2020-01-06 10:33:36'),(70,2,'更新渠道[云来支付]状态为:关闭',5,'127.0.0.1','2020-01-06 10:59:13'),(71,2,'更新渠道[云来支付]状态为:开启',5,'127.0.0.1','2020-01-06 10:59:15'),(72,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-06 13:43:03'),(73,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-06 13:44:14'),(74,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-06 15:46:23'),(75,2,'修改商户[1000950100]信息',5,'127.0.0.1','2020-01-06 16:08:48'),(76,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-06 17:50:50'),(77,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-07 14:10:59'),(78,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-07 17:41:18'),(79,2,'更新渠道[云来支付]状态为:关闭',5,'127.0.0.1','2020-01-07 17:44:53'),(80,2,'更新渠道[云来支付]状态为:开启',5,'127.0.0.1','2020-01-07 17:45:03'),(81,2,'更新渠道[云来支付-支付宝扫码]状态:渠道已关闭',5,'127.0.0.1','2020-01-07 17:45:06'),(82,2,'更新渠道[云来支付-支付宝扫码]状态:渠道已开启',5,'127.0.0.1','2020-01-07 17:45:15'),(83,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-07 18:25:31'),(84,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-08 10:30:50'),(85,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-08 14:38:26'),(86,2,'给商户[1000950100]强制入账483.500元,订单号:[H2020010815372595817423]',5,'127.0.0.1','2020-01-08 15:37:45'),(87,2,'修改商户号[1000950100]商户状态为:禁用',5,'127.0.0.1','2020-01-08 15:39:28'),(88,2,'修改商户号[1000950100]商户状态为:禁用',5,'127.0.0.1','2020-01-08 15:39:35'),(89,2,'锁定出款订单[H2020010815393048173297]',5,'127.0.0.1','2020-01-08 15:43:57'),(90,2,'解除出款订单[H2020010815393048173297]',5,'127.0.0.1','2020-01-08 15:44:18'),(91,2,'锁定出款订单[H2020010815393048173297]',5,'127.0.0.1','2020-01-08 15:44:49'),(92,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-08 15:49:18'),(93,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-09 13:33:16'),(94,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-09 17:53:02'),(95,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-10 13:44:40'),(96,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-11 14:09:18'),(97,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-11 18:45:58'),(98,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-13 10:18:56'),(99,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-13 12:36:48'),(100,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-13 14:02:17'),(101,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-13 14:36:49'),(102,1,'修改商户号[1000950100]商户入金状态为:开启',1,'127.0.0.1','2020-01-13 14:55:26'),(103,1,'修改商户号[1000950100]商户入金状态为:禁用',1,'127.0.0.1','2020-01-13 14:57:00'),(104,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-13 15:09:14'),(105,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-13 15:14:34'),(106,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-13 19:08:03'),(107,1,'账号[admin]登录后台',1,'127.0.0.1','2020-01-14 13:38:14'),(108,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-14 13:38:34'),(109,2,'修改商户号[1000950100]商户入金状态为:开启',5,'127.0.0.1','2020-01-14 13:46:03'),(110,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-14 16:41:50'),(111,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-14 19:07:10'),(112,2,'账号[caiwu001]登录后台',5,'127.0.0.1','2020-01-15 13:38:21');

/*Table structure for table `mm_mch` */

DROP TABLE IF EXISTS `mm_mch`;

CREATE TABLE `mm_mch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch` int(10) NOT NULL COMMENT '商户号',
  `mch_name` varchar(20) NOT NULL COMMENT '商户名称',
  `password` varchar(32) NOT NULL COMMENT '登录密码',
  `md5key` varchar(32) NOT NULL COMMENT '秘钥',
  `balance` decimal(10,3) DEFAULT '0.000' COMMENT '余额',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1、正常 2、禁止',
  `rukuan` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '入金：1、正常 2、禁止',
  `chukuan` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '下发：1、正常 2、禁止',
  `freeze` decimal(10,3) DEFAULT '0.000' COMMENT '冻结金额',
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '代理商id',
  `mobile` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `is_agent` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '2、代理 1、商户',
  `googl_secret` varchar(20) DEFAULT NULL COMMENT '谷歌秘钥',
  `agent_rate` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '代理费率',
  `secret_key` int(7) unsigned DEFAULT NULL COMMENT '随机数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `商户号` (`mch`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch` */

insert  into `mm_mch`(`id`,`mch`,`mch_name`,`password`,`md5key`,`balance`,`status`,`rukuan`,`chukuan`,`freeze`,`agent_id`,`mobile`,`email`,`create_time`,`is_agent`,`googl_secret`,`agent_rate`,`secret_key`) values (1,1000950100,'技术测试','4yMg1zZWX1i4z+MHzxYNpw==','eb82228dada9cd21679832222dcc399c','1543.900',1,1,2,'0.000',2,'','','2019-12-31 14:36:40',1,NULL,'0.0000',38947890),(2,1000886100,'古风','4yMg1zZWX1i4z+MHzxYNpw==','8d9be90cd1028b2cd142bcc47011f512','0.000',1,2,2,'0.000',0,'','','2019-12-31 14:39:37',2,NULL,'0.0350',69763041),(9,1000119700,'测试','4yMg1zZWX1i4z+MHzxYNpw==','8d4jchm7sntlka2hbpq9wkw318ugkwhv','0.000',1,2,2,'0.000',0,NULL,NULL,'2020-01-01 12:13:36',1,NULL,'0.0000',54199093);

/*Table structure for table `mm_mch_channel` */

DROP TABLE IF EXISTS `mm_mch_channel`;

CREATE TABLE `mm_mch_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` varchar(11) DEFAULT NULL COMMENT '商户号',
  `payment` text COMMENT '支付类型id:运营费率:代理商费率:权重',
  `code` varchar(30) DEFAULT NULL COMMENT '支付编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch_channel` */

insert  into `mm_mch_channel`(`id`,`mch_id`,`payment`,`code`) values (1,'1000950100','1:0.033:0:1:0','aliqr');

/*Table structure for table `mm_mch_log` */

DROP TABLE IF EXISTS `mm_mch_log`;

CREATE TABLE `mm_mch_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mid` varchar(11) NOT NULL,
  `ip` varchar(18) DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `action` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch_log` */

insert  into `mm_mch_log`(`id`,`mid`,`ip`,`type`,`action`,`create_time`) values (1,'1000119700','127.0.0.1',1,'登录商户后台','2020-01-04 16:02:48'),(2,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-04 16:03:00'),(3,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-06 15:46:42'),(4,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-06 16:09:00'),(5,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-06 18:29:30'),(6,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-06 18:29:31'),(7,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-08 15:37:56'),(8,'1000950100','127.0.0.1',1,'申请提现,申请金额：100元','2020-01-08 15:39:30'),(9,'1000119700','127.0.0.1',1,'登录商户后台','2020-01-09 15:41:32'),(10,'1000119700','127.0.0.1',1,'登录商户后台','2020-01-09 15:41:45'),(11,'1000119700','127.0.0.1',1,'登录商户后台','2020-01-09 15:42:01'),(12,'1000119700','127.0.0.1',1,'登录商户后台','2020-01-09 17:46:13'),(13,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-13 10:19:19'),(14,'1000886100','127.0.0.1',1,'修改商户号[1000950100]商户出款状态为:开启','2020-01-13 10:47:27'),(15,'1000886100','127.0.0.1',1,'修改商户号[1000950100]商户出款状态为:禁用','2020-01-13 10:47:43'),(16,'1000886100','127.0.0.1',1,'修改商户号[1000950100]商户入金状态为:禁用','2020-01-13 10:47:45'),(17,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-13 15:11:29'),(18,'1000886100','127.0.0.1',2,'登录商户后台','2020-01-13 15:12:06'),(19,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 18:50:46'),(20,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 18:51:09'),(21,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 18:53:10'),(22,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 18:55:19'),(23,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 18:55:55'),(24,'1000950100','127.0.0.1',1,'登录商户后台','2020-01-14 19:06:38');

/*Table structure for table `mm_mch_record` */

DROP TABLE IF EXISTS `mm_mch_record`;

CREATE TABLE `mm_mch_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` varchar(11) NOT NULL COMMENT '商户号',
  `balance` decimal(10,3) DEFAULT NULL COMMENT '变更金额',
  `total_balance` decimal(10,3) DEFAULT NULL COMMENT '变更后的余额',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `type` tinyint(5) unsigned DEFAULT '1' COMMENT '1、增加 2、减少 3、冻结 4、解冻 5、支付',
  `order_sn` varchar(32) DEFAULT NULL COMMENT '订单号',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch_record` */

insert  into `mm_mch_record`(`id`,`mch_id`,`balance`,`total_balance`,`remark`,`type`,`order_sn`,`ip`,`create_time`) values (1,'1000950100','96.700','96.700','强制入账',6,'H2019123115334984078952','127.0.0.1','2020-01-05 19:17:29'),(2,'1000950100','483.500','580.200','强制入账',6,'H2020010316020687382335','127.0.0.1','2020-01-05 19:18:53'),(3,'1000950100','483.500','1063.700','强制入账',6,'H2020010316020687382335','127.0.0.1','2020-01-05 19:25:23'),(17,'1000950100','96.700','1160.400','强制入账',6,'H2019123115334984078952','127.0.0.1','2020-01-05 19:47:51'),(18,'1000950100','483.500','1643.900','强制入账',6,'H2020010815372595817423','127.0.0.1','2020-01-08 15:37:44'),(19,'1000950100','100.000','1543.900','申请提现',7,'H2020010815393048173297','127.0.0.1','2020-01-08 15:39:30');

/*Table structure for table `mm_mch_white_ip` */

DROP TABLE IF EXISTS `mm_mch_white_ip`;

CREATE TABLE `mm_mch_white_ip` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` varchar(11) NOT NULL,
  `white_ip` text,
  `remark` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch_white_ip` */

insert  into `mm_mch_white_ip`(`id`,`mch_id`,`white_ip`,`remark`,`create_time`) values (2,'1000950100','127.0.0.1','','2020-01-14 13:44:41'),(3,'1000119700','127.0.0.1','','2020-01-14 13:45:01');

/*Table structure for table `mm_mch_withdrawal_ip` */

DROP TABLE IF EXISTS `mm_mch_withdrawal_ip`;

CREATE TABLE `mm_mch_withdrawal_ip` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` varchar(11) NOT NULL,
  `white_ip` text NOT NULL,
  `remark` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_mch_withdrawal_ip` */

insert  into `mm_mch_withdrawal_ip`(`id`,`mch_id`,`white_ip`,`remark`,`create_time`) values (1,'1000950100','127.0.0.1','','2019-12-31 15:33:05');

/*Table structure for table `mm_menu` */

DROP TABLE IF EXISTS `mm_menu`;

CREATE TABLE `mm_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL COMMENT '名称',
  `action` varchar(100) DEFAULT NULL COMMENT '路由',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单id',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '2显示 1隐藏',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1后台菜单 2商户菜单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

/*Data for the table `mm_menu` */

insert  into `mm_menu`(`id`,`title`,`action`,`pid`,`icon`,`type`,`sort`,`create_time`,`update_time`,`status`) values (1,'订单管理','',0,'fa fa-desktop',2,9,'2019-12-30 14:29:20','2020-01-11 19:11:19',1),(2,'商户管理',NULL,0,'fa fa-user',2,8,'2019-12-30 14:29:20','2020-01-11 19:11:24',1),(3,'出款管理',NULL,0,'fa fa-credit-card',2,6,'2019-12-30 14:29:20','2020-01-11 19:13:07',1),(4,'渠道管理',NULL,0,'fa fa-list',2,5,'2019-12-30 14:29:20','2020-01-11 19:13:11',1),(5,'系统管理',NULL,0,'fa fa-gears',2,3,'2019-12-30 14:29:20','2020-01-11 19:13:26',1),(6,'角色列表','manage/role',5,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(7,'账号管理','manage/account',5,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(8,'编辑权限','manage/roleEdit',6,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(9,'渠道列表','manage/channel',4,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(10,'支付类型','manage/payment',4,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(11,'交易记录','manage/order',1,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(12,'出款记录','manage/with',3,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(13,'商户列表','manage/user',2,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(15,'删除','manage/delAccount',7,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(16,'添加/编辑','manage/editAccount',7,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(17,'状态','manage/account_status',7,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(18,'添加商户','manage/user/addMch',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(19,'代理商列表','manage/user/agent',76,NULL,2,0,'2019-12-30 14:29:20','2020-01-06 11:20:27',1),(20,'编辑','manage/mchEdit',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(21,'资金变动记录','manage/record',2,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(22,'添加','manage/addChannel',9,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(23,'编辑','manage/editChannel',9,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(24,'渠道状态','manage/channelState',9,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(25,'商户状态','manage/statusMch',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(26,'入金','manage/statusMch',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(27,'出款','manage/statusMch',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(28,'加/减余额','manage/change_balance',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(29,'冻结/解冻','manage/change_balance',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(34,'状态','manage/stateType',10,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(32,'添加/编辑','manage/addPayment',10,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(60,'删除','manage/delPayment',10,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(35,'配置渠道','manage/configChannel',13,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(51,'商户提现IP','manage/withdrawal_ip',48,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(50,'IP黑名单','manage/blacklist',48,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(48,'IP管理',NULL,0,'fa fa-street-view',2,2,'2019-12-30 14:29:20','2020-01-11 19:13:30',1),(39,'提现申请','manage/withRequest',12,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(40,'出款/拒绝','manage/operation',12,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(41,'后台日志','manage/operationLog',72,NULL,2,0,'2019-12-30 14:29:20','2020-01-06 11:06:27',1),(42,'数据统计',NULL,0,'fa fa-line-chart',2,1,'2019-12-30 14:29:20','2020-01-11 19:11:59',1),(43,'收益明细','manage/detail',76,NULL,2,0,'2019-12-30 14:29:20','2020-01-11 18:56:14',1),(44,'渠道统计','manage/chanStatistical',42,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(45,'商户统计','manage/mchStatistical',42,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(46,'平台统计','manage/platformStatistical',42,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(47,'锁定/解除','manage/wit_is_lock',12,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(52,'商户下单IP','manage/with_blacklist',48,NULL,2,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(53,'添加/删除','manage/blacklist_add',50,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(54,'添加/更新/删除','manage/mch_ip_save',52,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(55,'编辑','manage/mch_ip_edit/pid',52,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(56,'添加/更新/删除','manage/withdrawal_ip_save',51,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(57,'编辑','manage/with_ip_edit/pid',51,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(58,'配置渠道','manage/agentConf',19,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(59,'删除','manage/delChan',9,NULL,1,0,'2019-12-30 14:29:20','2019-12-30 14:28:29',1),(61,'补发通知','manage/notice',11,NULL,1,0,'2019-12-30 14:29:20','2020-01-11 19:14:09',1),(66,'添加','manage/add_menu',74,NULL,2,0,'2019-12-30 18:58:24','2020-01-06 11:14:22',1),(63,'详情','manage/orderDetails',11,NULL,1,0,'2019-12-30 14:29:20','2020-01-06 10:58:46',1),(76,'代理商',NULL,0,'fa fa-users',2,7,'2020-01-06 11:19:15','2020-01-11 19:17:20',1),(67,'排序','manage/menuSort',74,NULL,2,0,'2019-12-30 18:58:50','2020-01-06 11:14:24',1),(68,'删除','manage/meunDel/pid',74,NULL,2,0,'2019-12-30 18:59:40','2020-01-06 11:14:26',1),(69,'开启/隐藏','manage/system/menu_type',74,NULL,2,0,'2019-12-30 19:00:32','2020-01-06 11:14:30',1),(70,'商户日志','manage/mchLog',72,NULL,2,0,'2020-01-03 14:23:05','2020-01-06 11:05:56',1),(71,'强制入单','manage/compel',11,NULL,2,0,'2020-01-05 19:13:48','2020-01-05 19:13:48',1),(72,'日志管理',NULL,0,'fa fa-comment',2,4,'2020-01-06 11:05:15','2020-01-11 19:13:19',1),(81,'订单管理',NULL,0,'fa fa-desktop',2,0,'2020-01-06 15:47:24','2020-01-09 17:58:00',2),(74,'菜单列表','manage/menu',5,NULL,2,0,'2020-01-06 11:11:33','2020-01-06 13:42:54',1),(82,'资金管理',NULL,0,'fa fa-credit-card',2,0,'2020-01-06 15:56:22','2020-01-06 15:56:22',2),(83,'API管理',NULL,0,'fa fa-file-text',2,0,'2020-01-06 15:56:56','2020-01-06 15:56:56',2),(84,'提现记录','web/withdrawal',82,NULL,2,0,'2020-01-06 16:05:54','2020-01-06 16:05:54',2),(85,'银行卡列表','web/bank',82,NULL,2,0,'2020-01-06 16:06:32','2020-01-06 16:06:32',2),(86,'交易记录','web/order',81,NULL,2,0,'2020-01-06 16:06:54','2020-01-09 17:58:01',2),(87,'商户管理',NULL,0,'fa fa fa-user',2,0,'2020-01-06 16:08:03','2020-01-11 19:16:24',2),(88,'下级商户','web/user',87,NULL,2,0,'2020-01-06 16:09:35','2020-01-06 16:09:35',2),(89,'对接文档','web/document',83,NULL,2,0,'2020-01-06 16:10:23','2020-01-06 16:10:23',2),(90,'商户信息','web/material',83,NULL,2,0,'2020-01-06 16:10:48','2020-01-06 16:10:48',2),(91,'状态/入金/出款','web/user/is_mch',88,NULL,1,0,'2020-01-06 16:15:38','2020-01-06 16:15:45',2),(99,'渠道配置','web/user/configChannel',88,NULL,1,0,'2020-01-07 14:21:41','2020-01-07 14:21:44',2),(93,'重置秘钥','web/reset_md5key',90,NULL,2,0,'2020-01-06 18:28:43','2020-01-06 18:28:43',2),(94,'添加/编辑','web/save_bank',85,NULL,1,0,'2020-01-06 18:30:26','2020-01-06 18:31:19',2),(95,'删除','web/del_bank',85,NULL,1,0,'2020-01-06 18:30:43','2020-01-06 18:31:15',2),(96,'提现申请','web/withRequest',84,NULL,1,0,'2020-01-06 18:31:04','2020-01-06 18:31:23',2),(97,'商户资金变更','web/water',81,NULL,2,0,'2020-01-06 18:55:01','2020-01-09 17:58:05',2),(98,'代理资金变更','web/agent_water',81,NULL,2,0,'2020-01-06 18:55:22','2020-01-09 17:58:15',2),(100,'编辑','manage/menu_edit',74,NULL,2,0,'2020-01-07 14:26:24','2020-01-07 14:26:24',1),(102,'编辑渠道','manage/chanEdit',12,NULL,1,0,'2020-01-08 15:43:18','2020-01-08 15:43:32',1);

/*Table structure for table `mm_order` */

DROP TABLE IF EXISTS `mm_order`;

CREATE TABLE `mm_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` varchar(11) NOT NULL COMMENT '商户号',
  `out_trade_no` varchar(30) NOT NULL COMMENT '商户订单号',
  `systen_no` varchar(30) NOT NULL COMMENT '平台订单号',
  `transaction_no` varchar(30) DEFAULT NULL COMMENT '三方流水号',
  `amount` decimal(10,3) NOT NULL COMMENT '下单金额',
  `actual_amount` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '实际支付',
  `cost_rate` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '成本费率',
  `run_rate` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '运营费率',
  `total_fee` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '运营手续费',
  `settle` decimal(10,3) NOT NULL COMMENT '商户结算',
  `agent_rate` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '代理费率',
  `upstream_settle` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '上游结算',
  `agent_amount` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '代理商结算',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `payment_id` int(11) unsigned NOT NULL COMMENT '支付类型id',
  `pay_code` varchar(10) DEFAULT NULL COMMENT '支付编码',
  `callback_url` varchar(200) NOT NULL COMMENT '异步回调',
  `return_url` varchar(200) DEFAULT NULL COMMENT '同步跳转',
  `pay_status` tinyint(1) DEFAULT '1' COMMENT '支付状态 1未支付 2已支付',
  `notice` tinyint(1) DEFAULT '1' COMMENT '1、未回调 2、已回调',
  `goods` varchar(100) DEFAULT NULL COMMENT '商品描述',
  `ip` varchar(20) DEFAULT NULL COMMENT '请求ip',
  `Platform` decimal(10,3) DEFAULT '0.000' COMMENT '平台收益',
  `remark` text COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `is_manual` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '2手动 回调1、自动回调',
  PRIMARY KEY (`id`),
  UNIQUE KEY `system_no` (`systen_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `mm_order` */

insert  into `mm_order`(`id`,`mch_id`,`out_trade_no`,`systen_no`,`transaction_no`,`amount`,`actual_amount`,`cost_rate`,`run_rate`,`total_fee`,`settle`,`agent_rate`,`upstream_settle`,`agent_amount`,`pay_time`,`channel_id`,`payment_id`,`pay_code`,`callback_url`,`return_url`,`pay_status`,`notice`,`goods`,`ip`,`Platform`,`remark`,`create_time`,`is_manual`) values (1,'1000950100','1577777629','H2019123115334984078952','','100.000','100.000','0.023','0.033','3.300','96.700','0.000','97.700','0.000','2020-01-05 19:47:51',4,1,'aliqr','http://www.pay.cn/pay/test/notify','http://www.pay.cn/pay/test/callback',2,2,'wlf','127.0.0.1','1.000','强制入账','2019-12-31 15:33:49',2),(2,'1000950100','1578038526','H2020010316020687382335','','500.000','500.000','0.023','0.033','16.500','483.500','0.000','488.500','0.000','2020-01-05 19:25:23',4,1,'aliqr','http://www.pay.cn/pay/test/notify','http://www.pay.cn/pay/test/callback',2,2,'wlf','127.0.0.1','5.000','强制入账','2020-01-03 16:02:06',2),(3,'1000950100','1578390318','H2020010717451891843868',NULL,'100.000','0.000','0.023','0.033','3.300','96.700','0.000','97.700','0.000',NULL,4,1,'aliqr','http://www.pay.cn/pay/test/notify','http://www.pay.cn/pay/test/callback',1,1,'wlf','127.0.0.1','1.000',NULL,'2020-01-07 17:45:18',1),(4,'1000950100','1578469045','H2020010815372595817423','','500.000','500.000','0.023','0.033','16.500','483.500','0.000','488.500','0.000','2020-01-08 15:37:44',4,1,'aliqr','http://www.pay.cn/pay/test/notify','http://www.pay.cn/pay/test/callback',2,2,'wlf','127.0.0.1','5.000','强制入账','2020-01-08 15:37:25',2);

/*Table structure for table `mm_payment` */

DROP TABLE IF EXISTS `mm_payment`;

CREATE TABLE `mm_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) unsigned DEFAULT NULL COMMENT '渠道id',
  `code` varchar(15) DEFAULT NULL COMMENT '渠道编码',
  `pay_type` varchar(10) DEFAULT NULL COMMENT '支付方式',
  `cost` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '成本',
  `min_amount` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '最小金额',
  `max_amount` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '最大金额',
  `fixed_amount` varchar(255) NOT NULL DEFAULT '0.000' COMMENT '固定金额',
  `double_amount` int(11) NOT NULL DEFAULT '0' COMMENT '倍数金额',
  `exclude_tail` varchar(255) NOT NULL DEFAULT '0' COMMENT '排除尾数',
  `qualified` int(110) unsigned NOT NULL DEFAULT '0' COMMENT '当日限额',
  `status` tinyint(1) unsigned zerofill NOT NULL DEFAULT '1' COMMENT '1、开启 2、关闭',
  `settlement` tinyint(1) NOT NULL DEFAULT '1' COMMENT '结算方式：1、D0 2、T1',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_payment` */

insert  into `mm_payment`(`id`,`channel_id`,`code`,`pay_type`,`cost`,`min_amount`,`max_amount`,`fixed_amount`,`double_amount`,`exclude_tail`,`qualified`,`status`,`settlement`,`create_time`,`update_time`) values (1,4,'aliqr','aliqr','0.023','100.000','5000.000','',0,'',0,1,1,'2020-01-03 16:01:59','2020-01-07 17:45:15');

/*Table structure for table `mm_role` */

DROP TABLE IF EXISTS `mm_role`;

CREATE TABLE `mm_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) DEFAULT NULL,
  `action` text COMMENT '菜单id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `mm_role` */

insert  into `mm_role`(`id`,`role_name`,`action`,`create_time`) values (1,'超级管理员',NULL,'2019-11-08 18:52:26'),(2,'其他','1,11','2019-12-29 13:44:07'),(3,'运营',NULL,'2019-11-12 19:10:22'),(4,'客服',NULL,'2019-11-08 18:54:28'),(5,'财务','1,11,61,63,71,2,13,18,20,25,26,27,28,29,35,21,3,12,39,40,47,102,4,9,22,23,24,59,10,34,32,60,5,6,8,7,15,16,17,74,66,67,68,69,100,48,51,56,57,50,53,52,54,55,42,44,45,46,76,19,58,43,72,41,70','2020-01-13 10:57:16'),(6,'商户','81,86,97,82,84,96,85,94,95,83,89,90,93','2020-01-09 18:57:31'),(7,'代理商','81,86,98,82,84,96,85,94,95,87,88,99','2020-01-13 10:56:05');

/*Table structure for table `mm_white_ip` */

DROP TABLE IF EXISTS `mm_white_ip`;

CREATE TABLE `mm_white_ip` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `white_ip` varchar(20) DEFAULT NULL,
  `remark` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `mm_white_ip` */

/*Table structure for table `mm_withdrawal` */

DROP TABLE IF EXISTS `mm_withdrawal`;

CREATE TABLE `mm_withdrawal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `with_out_no` varchar(32) NOT NULL COMMENT '订单号',
  `mch_id` int(11) NOT NULL COMMENT '商户号',
  `bank_id` int(11) NOT NULL COMMENT '银行卡id',
  `poundage` decimal(10,2) DEFAULT NULL COMMENT '手续费',
  `total_fee` decimal(10,3) DEFAULT NULL COMMENT '申请金额',
  `actual_fee` decimal(10,3) DEFAULT NULL COMMENT '实际到账接',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1、未处理2、处理中 3、已出款 4、出款失败',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1、商户 2、代理商',
  `channel` varchar(50) DEFAULT NULL COMMENT '出款渠道',
  `remark` text COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `lock` tinyint(1) DEFAULT '1' COMMENT '1、解除 2、已锁定',
  `lock_name` varchar(20) DEFAULT NULL COMMENT '锁定账号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `mm_withdrawal` */

insert  into `mm_withdrawal`(`id`,`with_out_no`,`mch_id`,`bank_id`,`poundage`,`total_fee`,`actual_fee`,`status`,`type`,`channel`,`remark`,`create_time`,`update_time`,`lock`,`lock_name`) values (1,'H2020010815393048173297',1000950100,1,'5.00','100.000','95.000',2,1,'云来支付',NULL,'2020-01-08 15:39:30','2020-01-08 15:44:49',2,'caiwu001');

/*Table structure for table `mm_chan_statistical` */

DROP TABLE IF EXISTS `mm_chan_statistical`;

/*!50001 DROP VIEW IF EXISTS `mm_chan_statistical` */;
/*!50001 DROP TABLE IF EXISTS `mm_chan_statistical` */;

/*!50001 CREATE TABLE  `mm_chan_statistical`(
 `channel_id` int(11) ,
 `pay_code` varchar(10) ,
 `create_time` varchar(10) ,
 `amount` decimal(32,3) ,
 `max_count` bigint(21) ,
 `actual_amount` decimal(32,3) ,
 `succ_num` decimal(23,0) ,
 `err_pay_amount` decimal(32,3) ,
 `error_num` decimal(23,0) ,
 `settle` decimal(32,3) ,
 `agent_amount` decimal(32,3) ,
 `upstream_settle` decimal(32,3) ,
 `platform` decimal(32,3) 
)*/;

/*Table structure for table `mm_mch_statistical` */

DROP TABLE IF EXISTS `mm_mch_statistical`;

/*!50001 DROP VIEW IF EXISTS `mm_mch_statistical` */;
/*!50001 DROP TABLE IF EXISTS `mm_mch_statistical` */;

/*!50001 CREATE TABLE  `mm_mch_statistical`(
 `mch_id` varchar(11) ,
 `max_amount` decimal(32,3) ,
 `max_count` bigint(21) ,
 `create_time` varchar(10) ,
 `total_fee` decimal(32,3) ,
 `actual_amount` decimal(32,3) ,
 `succ_num` decimal(23,0) ,
 `pay_amount` decimal(32,3) ,
 `err_num` decimal(23,0) ,
 `settle` decimal(32,3) 
)*/;

/*Table structure for table `mm_mch_withdrawal` */

DROP TABLE IF EXISTS `mm_mch_withdrawal`;

/*!50001 DROP VIEW IF EXISTS `mm_mch_withdrawal` */;
/*!50001 DROP TABLE IF EXISTS `mm_mch_withdrawal` */;

/*!50001 CREATE TABLE  `mm_mch_withdrawal`(
 `create_time` varchar(10) ,
 `mch` int(10) ,
 `untreated_amount` decimal(32,3) ,
 `untreated_num` decimal(23,0) ,
 `waiting_amount` decimal(32,3) ,
 `waiting_num` decimal(23,0) ,
 `complete_amount` decimal(32,3) ,
 `complete_num` decimal(23,0) ,
 `total_amount` decimal(32,3) ,
 `total_amount_num` bigint(21) ,
 `refund_amount` decimal(32,3) ,
 `refund_num` decimal(23,0) 
)*/;

/*Table structure for table `mm_platformh_statistical` */

DROP TABLE IF EXISTS `mm_platformh_statistical`;

/*!50001 DROP VIEW IF EXISTS `mm_platformh_statistical` */;
/*!50001 DROP TABLE IF EXISTS `mm_platformh_statistical` */;

/*!50001 CREATE TABLE  `mm_platformh_statistical`(
 `create_time` varchar(10) ,
 `max_amount` decimal(32,3) ,
 `max_count` bigint(21) ,
 `waiting_amount` decimal(32,3) ,
 `swaiting_num` decimal(23,0) ,
 `actual_amount` decimal(32,3) ,
 `succ_num` decimal(23,0) ,
 `err_amount` decimal(32,3) ,
 `err_num` decimal(23,0) ,
 `settle` decimal(32,3) ,
 `agent_amount` decimal(32,3) ,
 `upstream_settle` decimal(32,3) ,
 `platform` decimal(32,3) 
)*/;

/*Table structure for table `mm_with` */

DROP TABLE IF EXISTS `mm_with`;

/*!50001 DROP VIEW IF EXISTS `mm_with` */;
/*!50001 DROP TABLE IF EXISTS `mm_with` */;

/*!50001 CREATE TABLE  `mm_with`(
 `create_time` varchar(10) ,
 `untreated_amount` decimal(32,3) ,
 `untreated_num` decimal(23,0) ,
 `waiting_amount` decimal(32,3) ,
 `waiting_num` decimal(23,0) ,
 `poundage` decimal(32,2) ,
 `complete_amount` decimal(32,3) ,
 `complete_num` decimal(23,0) ,
 `total_amount` decimal(32,3) ,
 `total_amount_num` bigint(21) ,
 `refund_amount` decimal(32,3) ,
 `refund_num` decimal(23,0) 
)*/;

/*View structure for view mm_chan_statistical */

/*!50001 DROP TABLE IF EXISTS `mm_chan_statistical` */;
/*!50001 DROP VIEW IF EXISTS `mm_chan_statistical` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mm_chan_statistical` AS select `mm_order`.`channel_id` AS `channel_id`,`mm_order`.`pay_code` AS `pay_code`,date_format(`mm_order`.`create_time`,'%Y-%m-%d') AS `create_time`,sum(`mm_order`.`amount`) AS `amount`,count(`mm_order`.`id`) AS `max_count`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`actual_amount`,0)) AS `actual_amount`,sum(if((`mm_order`.`pay_status` = 2),1,0)) AS `succ_num`,sum(if((`mm_order`.`pay_status` in (1,3)),`mm_order`.`amount`,0)) AS `err_pay_amount`,sum(if((`mm_order`.`pay_status` in (1,3)),1,0)) AS `error_num`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`settle`,0)) AS `settle`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`agent_amount`,0)) AS `agent_amount`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`upstream_settle`,0)) AS `upstream_settle`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`Platform`,0)) AS `platform` from `mm_order` group by `mm_order`.`channel_id`,`mm_order`.`payment_id`,`mm_order`.`pay_code`,date_format(`mm_order`.`create_time`,'%Y-%m-%d') order by `create_time` desc */;

/*View structure for view mm_mch_statistical */

/*!50001 DROP TABLE IF EXISTS `mm_mch_statistical` */;
/*!50001 DROP VIEW IF EXISTS `mm_mch_statistical` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mm_mch_statistical` AS select `mm_order`.`mch_id` AS `mch_id`,sum(`mm_order`.`amount`) AS `max_amount`,count(`mm_order`.`id`) AS `max_count`,date_format(`mm_order`.`create_time`,'%Y-%m-%d') AS `create_time`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`total_fee`,0)) AS `total_fee`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`actual_amount`,0)) AS `actual_amount`,sum(if((`mm_order`.`pay_status` = 2),1,0)) AS `succ_num`,sum(if((`mm_order`.`pay_status` <> 2),`mm_order`.`amount`,0)) AS `pay_amount`,sum(if((`mm_order`.`pay_status` <> 2),1,0)) AS `err_num`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`settle`,0)) AS `settle` from `mm_order` group by `mm_order`.`mch_id`,date_format(`mm_order`.`create_time`,'%Y-%m-%d') order by `create_time` desc */;

/*View structure for view mm_mch_withdrawal */

/*!50001 DROP TABLE IF EXISTS `mm_mch_withdrawal` */;
/*!50001 DROP VIEW IF EXISTS `mm_mch_withdrawal` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mm_mch_withdrawal` AS select date_format(`b`.`create_time`,'%Y-%m-%d') AS `create_time`,`a`.`mch` AS `mch`,sum(if((`b`.`status` = 1),`b`.`total_fee`,0)) AS `untreated_amount`,sum(if((`b`.`status` = 1),1,0)) AS `untreated_num`,sum(if((`b`.`status` = 2),`b`.`total_fee`,0)) AS `waiting_amount`,sum(if((`b`.`status` = 2),1,0)) AS `waiting_num`,sum(if((`b`.`status` = 3),`b`.`total_fee`,0)) AS `complete_amount`,sum(if((`b`.`status` = 3),1,0)) AS `complete_num`,sum(`b`.`total_fee`) AS `total_amount`,count(`b`.`status`) AS `total_amount_num`,sum(if((`b`.`status` = 4),`b`.`total_fee`,0)) AS `refund_amount`,sum(if((`b`.`status` = 4),1,0)) AS `refund_num` from (`mm_mch` `a` join `mm_withdrawal` `b` on((`a`.`mch` = `b`.`mch_id`))) group by `b`.`mch_id`,date_format(`b`.`create_time`,'%Y-%m-%d') */;

/*View structure for view mm_platformh_statistical */

/*!50001 DROP TABLE IF EXISTS `mm_platformh_statistical` */;
/*!50001 DROP VIEW IF EXISTS `mm_platformh_statistical` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mm_platformh_statistical` AS select date_format(`mm_order`.`create_time`,'%Y-%m-%d') AS `create_time`,sum(`mm_order`.`amount`) AS `max_amount`,count(`mm_order`.`id`) AS `max_count`,sum(if((`mm_order`.`pay_status` = 1),`mm_order`.`amount`,0)) AS `waiting_amount`,sum(if((`mm_order`.`pay_status` = 1),1,0)) AS `swaiting_num`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`actual_amount`,0)) AS `actual_amount`,sum(if((`mm_order`.`pay_status` = 2),1,0)) AS `succ_num`,sum(if((`mm_order`.`pay_status` = 3),`mm_order`.`amount`,0)) AS `err_amount`,sum(if((`mm_order`.`pay_status` = 3),1,0)) AS `err_num`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`settle`,0)) AS `settle`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`agent_amount`,0)) AS `agent_amount`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`upstream_settle`,0)) AS `upstream_settle`,sum(if((`mm_order`.`pay_status` = 2),`mm_order`.`Platform`,0)) AS `platform` from `mm_order` group by date_format(`mm_order`.`create_time`,'%Y-%m-%d') */;

/*View structure for view mm_with */

/*!50001 DROP TABLE IF EXISTS `mm_with` */;
/*!50001 DROP VIEW IF EXISTS `mm_with` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mm_with` AS select date_format(`mm_withdrawal`.`create_time`,'%Y-%m-%d') AS `create_time`,sum(if((`mm_withdrawal`.`status` = 1),`mm_withdrawal`.`total_fee`,0)) AS `untreated_amount`,sum(if((`mm_withdrawal`.`status` = 1),1,0)) AS `untreated_num`,sum(if((`mm_withdrawal`.`status` = 2),`mm_withdrawal`.`total_fee`,0)) AS `waiting_amount`,sum(if((`mm_withdrawal`.`status` = 2),1,0)) AS `waiting_num`,sum(if((`mm_withdrawal`.`status` = 3),`mm_withdrawal`.`poundage`,0)) AS `poundage`,sum(if((`mm_withdrawal`.`status` = 3),`mm_withdrawal`.`actual_fee`,0)) AS `complete_amount`,sum(if((`mm_withdrawal`.`status` = 3),1,0)) AS `complete_num`,sum(`mm_withdrawal`.`total_fee`) AS `total_amount`,count(`mm_withdrawal`.`total_fee`) AS `total_amount_num`,sum(if((`mm_withdrawal`.`status` = 4),`mm_withdrawal`.`total_fee`,0)) AS `refund_amount`,sum(if((`mm_withdrawal`.`status` = 4),1,0)) AS `refund_num` from `mm_withdrawal` group by date_format(`mm_withdrawal`.`create_time`,'%Y-%m-%d') */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
