# Thinkphp5.1

#### 介绍
Thinkphp5.1开发的一套支付系统，共有三个模块：web(商户、代理)模块、api(对外开放接口)模块和manage后台模块

#### 框架结构


#### 安装教程

1.  源码可直接运行

#### 使用说明

1.  服务器必须安装redis服务
2.  后台默认账号：admin 密码：123456
3.  路由功能必须强制开启

#### 后台样式

![输入图片说明](https://images.gitee.com/uploads/images/2020/0117/130907_56313122_5538598.png "840.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0117/123914_424aff34_5538598.png "3749.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0117/130638_35ede321_5538598.png "607.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0120/124943_f16a1285_5538598.png "19.png")