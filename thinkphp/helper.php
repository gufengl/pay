<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

//------------------------
// ThinkPHP 助手函数
//-------------------------

use think\Container;
use think\Db;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Cookie;
use think\facade\Debug;
use think\facade\Env;
use think\facade\Hook;
use think\facade\Lang;
use think\facade\Log;
use think\facade\Request;
use think\facade\Route;
use think\facade\Session;
use think\facade\Url;
use think\Response;
use think\route\RuleItem;

if (!function_exists('abort')) {
    /**
     * 抛出HTTP异常
     * @param integer|Response      $code 状态码 或者 Response对象实例
     * @param string                $message 错误信息
     * @param array                 $header 参数
     */
    function abort($code, $message = null, $header = [])
    {
        if ($code instanceof Response) {
            throw new HttpResponseException($code);
        } else {
            throw new HttpException($code, $message, null, $header);
        }
    }
}

if (!function_exists('action')) {
    /**
     * 调用模块的操作方法 参数格式 [模块/控制器/]操作
     * @param string        $url 调用地址
     * @param string|array  $vars 调用参数 支持字符串和数组
     * @param string        $layer 要调用的控制层名称
     * @param bool          $appendSuffix 是否添加类名后缀
     * @return mixed
     */
    function action($url, $vars = [], $layer = 'controller', $appendSuffix = false)
    {
        return app()->action($url, $vars, $layer, $appendSuffix);
    }
}

if (!function_exists('app')) {
    /**
     * 快速获取容器中的实例 支持依赖注入
     * @param string    $name 类名或标识 默认获取当前应用实例
     * @param array     $args 参数
     * @param bool      $newInstance    是否每次创建新的实例
     * @return mixed|\think\App
     */
    function app($name = 'think\App', $args = [], $newInstance = false)
    {
        return Container::get($name, $args, $newInstance);
    }
}

if (!function_exists('behavior')) {
    /**
     * 执行某个行为（run方法） 支持依赖注入
     * @param mixed $behavior   行为类名或者别名
     * @param mixed $args       参数
     * @return mixed
     */
    function behavior($behavior, $args = null)
    {
        return Hook::exec($behavior, $args);
    }
}

if (!function_exists('bind')) {
    /**
     * 绑定一个类到容器
     * @access public
     * @param string  $abstract    类标识、接口
     * @param mixed   $concrete    要绑定的类、闭包或者实例
     * @return Container
     */
    function bind($abstract, $concrete = null)
    {
        return Container::getInstance()->bindTo($abstract, $concrete);
    }
}

if (!function_exists('cache')) {
    /**
     * 缓存管理
     * @param mixed     $name 缓存名称，如果为数组表示进行缓存设置
     * @param mixed     $value 缓存值
     * @param mixed     $options 缓存参数
     * @param string    $tag 缓存标签
     * @return mixed
     */
    function cache($name, $value = '', $options = null, $tag = null)
    {
        if (is_array($options)) {
            // 缓存操作的同时初始化
            Cache::connect($options);
        } elseif (is_array($name)) {
            // 缓存初始化
            return Cache::connect($name);
        }

        if ('' === $value) {
            // 获取缓存
            return 0 === strpos($name, '?') ? Cache::has(substr($name, 1)) : Cache::get($name);
        } elseif (is_null($value)) {
            // 删除缓存
            return Cache::rm($name);
        }

        // 缓存数据
        if (is_array($options)) {
            $expire = isset($options['expire']) ? $options['expire'] : null; //修复查询缓存无法设置过期时间
        } else {
            $expire = is_numeric($options) ? $options : null; //默认快捷缓存设置过期时间
        }

        if (is_null($tag)) {
            return Cache::set($name, $value, $expire);
        } else {
            return Cache::tag($tag)->set($name, $value, $expire);
        }
    }
}

if (!function_exists('call')) {
    /**
     * 调用反射执行callable 支持依赖注入
     * @param mixed $callable   支持闭包等callable写法
     * @param array $args       参数
     * @return mixed
     */
    function call($callable, $args = [])
    {
        return Container::getInstance()->invoke($callable, $args);
    }
}

if (!function_exists('class_basename')) {
    /**
     * 获取类名(不包含命名空间)
     *
     * @param  string|object $class
     * @return string
     */
    function class_basename($class)
    {
        $class = is_object($class) ? get_class($class) : $class;
        return basename(str_replace('\\', '/', $class));
    }
}

if (!function_exists('class_uses_recursive')) {
    /**
     *获取一个类里所有用到的trait，包括父类的
     *
     * @param $class
     * @return array
     */
    function class_uses_recursive($class)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        $results = [];
        $classes = array_merge([$class => $class], class_parents($class));
        foreach ($classes as $class) {
            $results += trait_uses_recursive($class);
        }

        return array_unique($results);
    }
}

if (!function_exists('config')) {
    /**
     * 获取和设置配置参数
     * @param string|array  $name 参数名
     * @param mixed         $value 参数值
     * @return mixed
     */
    function config($name = '', $value = null)
    {
        if (is_null($value) && is_string($name)) {
            if ('.' == substr($name, -1)) {
                return Config::pull(substr($name, 0, -1));
            }

            return 0 === strpos($name, '?') ? Config::has(substr($name, 1)) : Config::get($name);
        } else {
            return Config::set($name, $value);
        }
    }
}

if (!function_exists('container')) {
    /**
     * 获取容器对象实例
     * @return Container
     */
    function container()
    {
        return Container::getInstance();
    }
}

if (!function_exists('controller')) {
    /**
     * 实例化控制器 格式：[模块/]控制器
     * @param string    $name 资源地址
     * @param string    $layer 控制层名称
     * @param bool      $appendSuffix 是否添加类名后缀
     * @return \think\Controller
     */
    function controller($name, $layer = 'controller', $appendSuffix = false)
    {
        return app()->controller($name, $layer, $appendSuffix);
    }
}

if (!function_exists('cookie')) {
    /**
     * Cookie管理
     * @param string|array  $name cookie名称，如果为数组表示进行cookie设置
     * @param mixed         $value cookie值
     * @param mixed         $option 参数
     * @return mixed
     */
    function cookie($name, $value = '', $option = null)
    {
        if (is_array($name)) {
            // 初始化
            Cookie::init($name);
        } elseif (is_null($name)) {
            // 清除
            Cookie::clear($value);
        } elseif ('' === $value) {
            // 获取
            return 0 === strpos($name, '?') ? Cookie::has(substr($name, 1), $option) : Cookie::get($name);
        } elseif (is_null($value)) {
            // 删除
            return Cookie::delete($name);
        } else {
            // 设置
            return Cookie::set($name, $value, $option);
        }
    }
}

if (!function_exists('db')) {
    /**
     * 实例化数据库类
     * @param string        $name 操作的数据表名称（不含前缀）
     * @param array|string  $config 数据库配置参数
     * @param bool          $force 是否强制重新连接
     * @return \think\db\Query
     */
    function db($name = '', $config = [], $force = true)
    {
        return Db::connect($config, $force)->name($name);
    }
}

if (!function_exists('debug')) {
    /**
     * 记录时间（微秒）和内存使用情况
     * @param string            $start 开始标签
     * @param string            $end 结束标签
     * @param integer|string    $dec 小数位 如果是m 表示统计内存占用
     * @return mixed
     */
    function debug($start, $end = '', $dec = 6)
    {
        if ('' == $end) {
            Debug::remark($start);
        } else {
            return 'm' == $dec ? Debug::getRangeMem($start, $end) : Debug::getRangeTime($start, $end, $dec);
        }
    }
}

if (!function_exists('download')) {
    /**
     * 获取\think\response\Download对象实例
     * @param string  $filename 要下载的文件
     * @param string  $name 显示文件名
     * @param bool    $content 是否为内容
     * @param integer $expire 有效期（秒）
     * @return \think\response\Download
     */
    function download($filename, $name = '', $content = false, $expire = 360, $openinBrowser = false)
    {
        return Response::create($filename, 'download')->name($name)->isContent($content)->expire($expire)->openinBrowser($openinBrowser);
    }
}

if (!function_exists('dump')) {
    /**
     * 浏览器友好的变量输出
     * @param mixed     $var 变量
     * @param boolean   $echo 是否输出 默认为true 如果为false 则返回输出字符串
     * @param string    $label 标签 默认为空
     * @return void|string
     */
    function dump($var, $echo = true, $label = null)
    {
        return Debug::dump($var, $echo, $label);
    }
}

if (!function_exists('env')) {
    /**
     * 获取环境变量值
     * @access public
     * @param  string    $name 环境变量名（支持二级 .号分割）
     * @param  string    $default  默认值
     * @return mixed
     */
    function env($name = null, $default = null)
    {
        return Env::get($name, $default);
    }
}

if (!function_exists('exception')) {
    /**
     * 抛出异常处理
     *
     * @param string    $msg  异常消息
     * @param integer   $code 异常代码 默认为0
     * @param string    $exception 异常类
     *
     * @throws Exception
     */
    function exception($msg, $code = 0, $exception = '')
    {
        $e = $exception ?: '\think\Exception';
        throw new $e($msg, $code);
    }
}

if (!function_exists('halt')) {
    /**
     * 调试变量并且中断输出
     * @param mixed      $var 调试变量或者信息
     */
    function halt($var)
    {
        dump($var);

        throw new HttpResponseException(new Response);
    }
}

if (!function_exists('input')) {
    /**
     * 获取输入数据 支持默认值和过滤
     * @param string    $key 获取的变量名
     * @param mixed     $default 默认值
     * @param string    $filter 过滤方法
     * @return mixed
     */
    function input($key = '', $default = null, $filter = '')
    {
        if (0 === strpos($key, '?')) {
            $key = substr($key, 1);
            $has = true;
        }

        if ($pos = strpos($key, '.')) {
            // 指定参数来源
            $method = substr($key, 0, $pos);
            if (in_array($method, ['get', 'post', 'put', 'patch', 'delete', 'route', 'param', 'request', 'session', 'cookie', 'server', 'env', 'path', 'file'])) {
                $key = substr($key, $pos + 1);
            } else {
                $method = 'param';
            }
        } else {
            // 默认为自动判断
            $method = 'param';
        }

        if (isset($has)) {
            return request()->has($key, $method, $default);
        } else {
            return request()->$method($key, $default, $filter);
        }
    }
}

if (!function_exists('json')) {
    /**
     * 获取\think\response\Json对象实例
     * @param mixed   $data 返回的数据
     * @param integer $code 状态码
     * @param array   $header 头部
     * @param array   $options 参数
     * @return \think\response\Json
     */
    function json($data = [], $code = 200, $header = [], $options = [])
    {
        return Response::create($data, 'json', $code, $header, $options);
    }
}

if (!function_exists('jsonp')) {
    /**
     * 获取\think\response\Jsonp对象实例
     * @param mixed   $data    返回的数据
     * @param integer $code    状态码
     * @param array   $header 头部
     * @param array   $options 参数
     * @return \think\response\Jsonp
     */
    function jsonp($data = [], $code = 200, $header = [], $options = [])
    {
        return Response::create($data, 'jsonp', $code, $header, $options);
    }
}

if (!function_exists('lang')) {
    /**
     * 获取语言变量值
     * @param string    $name 语言变量名
     * @param array     $vars 动态变量值
     * @param string    $lang 语言
     * @return mixed
     */
    function lang($name, $vars = [], $lang = '')
    {
        return Lang::get($name, $vars, $lang);
    }
}

if (!function_exists('model')) {
    /**
     * 实例化Model
     * @param string    $name Model名称
     * @param string    $layer 业务层名称
     * @param bool      $appendSuffix 是否添加类名后缀
     * @return \think\Model
     */
    function model($name = '', $layer = 'model', $appendSuffix = false)
    {
        return app()->model($name, $layer, $appendSuffix);
    }
}

if (!function_exists('parse_name')) {
    /**
     * 字符串命名风格转换
     * type 0 将Java风格转换为C的风格 1 将C风格转换为Java的风格
     * @param string  $name 字符串
     * @param integer $type 转换类型
     * @param bool    $ucfirst 首字母是否大写（驼峰规则）
     * @return string
     */
    function parse_name($name, $type = 0, $ucfirst = true)
    {
        if ($type) {
            $name = preg_replace_callback('/_([a-zA-Z])/', function ($match) {
                return strtoupper($match[1]);
            }, $name);

            return $ucfirst ? ucfirst($name) : lcfirst($name);
        } else {
            return strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $name), "_"));
        }
    }
}

if (!function_exists('redirect')) {
    /**
     * 获取\think\response\Redirect对象实例
     * @param mixed         $url 重定向地址 支持Url::build方法的地址
     * @param array|integer $params 额外参数
     * @param integer       $code 状态码
     * @return \think\response\Redirect
     */
    function redirect($url = [], $params = [], $code = 302)
    {
        if (is_integer($params)) {
            $code   = $params;
            $params = [];
        }

        return Response::create($url, 'redirect', $code)->params($params);
    }
}

if (!function_exists('request')) {
    /**
     * 获取当前Request对象实例
     * @return Request
     */
    function request()
    {
        return app('request');
    }
}

if (!function_exists('response')) {
    /**
     * 创建普通 Response 对象实例
     * @param mixed      $data   输出数据
     * @param int|string $code   状态码
     * @param array      $header 头信息
     * @param string     $type
     * @return Response
     */
    function response($data = '', $code = 200, $header = [], $type = 'html')
    {
        return Response::create($data, $type, $code, $header);
    }
}

if (!function_exists('route')) {
    /**
     * 路由注册
     * @param  string    $rule       路由规则
     * @param  mixed     $route      路由地址
     * @param  array     $option     路由参数
     * @param  array     $pattern    变量规则
     * @return RuleItem
     */
    function route($rule, $route, $option = [], $pattern = [])
    {
        return Route::rule($rule, $route, '*', $option, $pattern);
    }
}

if (!function_exists('session')) {
    /**
     * Session管理
     * @param string|array  $name session名称，如果为数组表示进行session设置
     * @param mixed         $value session值
     * @param string        $prefix 前缀
     * @return mixed
     */
    function session($name, $value = '', $prefix = null)
    {
        if (is_array($name)) {
            // 初始化
            Session::init($name);
        } elseif (is_null($name)) {
            // 清除
            Session::clear($value);
        } elseif ('' === $value) {
            // 判断或获取
            return 0 === strpos($name, '?') ? Session::has(substr($name, 1), $prefix) : Session::get($name, $prefix);
        } elseif (is_null($value)) {
            // 删除
            return Session::delete($name, $prefix);
        } else {
            // 设置
            return Session::set($name, $value, $prefix);
        }
    }
}

if (!function_exists('token')) {
    /**
     * 生成表单令牌
     * @param string $name 令牌名称
     * @param mixed  $type 令牌生成方法
     * @return string
     */
    function token($name = '__token__', $type = 'md5')
    {
        $token = Request::token($name, $type);

        return '<input type="hidden" name="' . $name . '" value="' . $token . '" />';
    }
}

if (!function_exists('trace')) {
    /**
     * 记录日志信息
     * @param mixed     $log log信息 支持字符串和数组
     * @param string    $level 日志级别
     * @return array|void
     */
    function trace($log = '[think]', $level = 'log')
    {
        if ('[think]' === $log) {
            return Log::getLog();
        } else {
            Log::record($log, $level);
        }
    }
}

if (!function_exists('trait_uses_recursive')) {
    /**
     * 获取一个trait里所有引用到的trait
     *
     * @param  string $trait
     * @return array
     */
    function trait_uses_recursive($trait)
    {
        $traits = class_uses($trait);
        foreach ($traits as $trait) {
            $traits += trait_uses_recursive($trait);
        }

        return $traits;
    }
}

if (!function_exists('url')) {
    /**
     * Url生成
     * @param string        $url 路由地址
     * @param string|array  $vars 变量
     * @param bool|string   $suffix 生成的URL后缀
     * @param bool|string   $domain 域名
     * @return string
     */
    function url($url = '', $vars = '', $suffix = true, $domain = false)
    {
        return Url::build($url, $vars, $suffix, $domain);
    }
}

if (!function_exists('validate')) {
    /**
     * 实例化验证器
     * @param string    $name 验证器名称
     * @param string    $layer 业务层名称
     * @param bool      $appendSuffix 是否添加类名后缀
     * @return \think\Validate
     */
    function validate($name = '', $layer = 'validate', $appendSuffix = false)
    {
        return app()->validate($name, $layer, $appendSuffix);
    }
}
if (!function_exists('get_client_ip')) {
    /*获取IP地址*/
    function get_client_ip($type = 0) {
        $type       =  $type ? 1 : 0;
        static $ip  =   NULL;
       if ($ip !== NULL) return $ip[$type];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
           $ip     =   $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }
}
if (!function_exists('view')) {
    /**
     * 渲染模板输出
     * @param string    $template 模板文件
     * @param array     $vars 模板变量
     * @param integer   $code 状态码
     * @param callable  $filter 内容过滤
     * @return \think\response\View
     */
    function view($template = '', $vars = [], $code = 200, $filter = null)
    {
        return Response::create($template, 'view', $code)->assign($vars)->filter($filter);
    }
}

if (!function_exists('widget')) {
    /**
     * 渲染输出Widget
     * @param string    $name Widget名称
     * @param array     $data 传入的参数
     * @return mixed
     */
    function widget($name, $data = [])
    {
        $result = app()->action($name, $data, 'widget');

        if (is_object($result)) {
            $result = $result->getContent();
        }

        return $result;
    }
}

if (!function_exists('xml')) {
    /**
     * 获取\think\response\Xml对象实例
     * @param mixed   $data    返回的数据
     * @param integer $code    状态码
     * @param array   $header  头部
     * @param array   $options 参数
     * @return \think\response\Xml
     */
    function xml($data = [], $code = 200, $header = [], $options = [])
    {
        return Response::create($data, 'xml', $code, $header, $options);
    }
}

if (!function_exists('yaconf')) {
    /**
     * 获取yaconf配置
     *
     * @param  string    $name 配置参数名
     * @param  mixed     $default   默认值
     * @return mixed
     */
    function yaconf($name, $default = null)
    {
        return Config::yaconf($name, $default);
    }
}
//清理缓存
if (!function_exists('delete_dir_file')){
    /**
     * 循环删除目录和文件
     * @param string $dir_name
     * @return bool
     */
    function delete_dir_file($dir_name) {
        $result = false;
        if(is_dir($dir_name)){
            if ($handle = opendir($dir_name)) {
                while (false !== ($item = readdir($handle))) {
                    if ($item != '.' && $item != '..') {
                        if (is_dir($dir_name . '/' . $item)) {
                            delete_dir_file($dir_name . '/' . $item);
                        } else {
                            unlink($dir_name . '/' . $item);
                        }
                    }
                }
                closedir($handle);
                if (rmdir($dir_name)) {
                    $result = true;
                }
            }
        }
        return $result;
    }
}
if (!function_exists('Googl_generate_url')) {
    /**
     * 生成谷歌验证码
     *
     * @param  string    $label 谷歌标识
     * @return mixed
     */
    function Googl_generate_url($label = '')
    {
        if(empty($label)){
            $label = $_SERVER['SERVER_NAME'];
        }
        $Googl = new \Google();
        //生成秘钥
        $secret = $Googl->createSecret();
       
        $url = $Googl->getQRCodeGoogleUrl($label,$secret);
        
        return ['url'=>$url,'googl_secret'=>$secret];
    }
}
/**
 * 绑定谷歌
 */
if(!function_exists('get_google')){

    function get_google($url,$lable=''){
        $google = Googl_generate_url($lable);
        $html = ' <link href="/static/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">';
        $html .= '<script src="/static/js/jquery.min.js"></script>';
        $html .= '<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>';
        $html .='<script>
                $(function(){
                    swal(
                        {
                        title:"",
                        text: "<div>手机打开谷歌扫一扫,绑定完请点击【确认绑定】</div>",
                        imageUrl: "'.$google['url'].'",
                        imageSize:"200x200",
                        confirmButtonText:"确认绑定",
                        html: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                        },
                        function (isConfirm){
                            if(isConfirm){
                               $.post("'.$url.'",{google_secret:"'.$google['googl_secret'].'"},function(result){
                                    swal({
                                        title:result.msg,
                                        timer: 2000,
                                        type:"success",
                                        showConfirmButton: false,
                                    });
                               });
                            }else{
                                swal({
                                    title:"绑定是失败",
                                    timer: 2000,
                                    type:"error",
                                    showConfirmButton: false,
                                });
                            }
                            setTimeout(function(){
                                location.reload();
                            },2000)
                        }
                    );
                })
            </script>';
        exit($html);
    }
}
if (!function_exists('Googl_validation')) {
    /**
     * 谷歌验证
     *
     * @param  string    $secret 谷歌秘钥
     * @param  mixed     $code   验证码
     * @return mixed
     */
    function Googl_validation($secret,$code)
    {
        $Googl = new \Google();

        $checkResult = $Googl->verifyCode($secret, $code, 2); 
        if ($checkResult) {
            return true;
        } else {
            return false;
        }
    }
}
/**
 * 生成二维码 返回base64图片格式
 */
if(!(function_exists('get_qrcode'))){
    function get_qrcode($url,$size = '4'){
        //引入phpqrcode类
        require_once dirname(__FILE__).'/../vendor/phpqrcode/phpqrcode.php';
        //实例化一个对象
        $Qrcode = new \QRcode();
        ob_start();
    
        $Qrcode->png($url,false,'L',$size,2);

        $base64img = base64_encode(ob_get_contents());

        ob_end_clean();

        return $base64img;
    }
}
/**
 * Undocumented 记录日志
 *
 * @param [type] $data
 * @param [type] $type
 * @return void
 */
if(!(function_exists('get_log'))){
    function get_log($data,$type='Yunlai'){
        $path = dirname(__FILE__).'/../runtime/log/tlogs/';
        if(!is_dir($path)) mkdir($path, 0755, true);
        $name = $path.date('Y-m-d') . ".log";
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new \DateTime (date('Y-m-d H:i:s.' . $micro, $t));
        if(is_array($data)){
            $data = json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        }
        file_put_contents($name, '[' . $d->format('Y-m-d H:i:s u') . "] " . $type . "\r\n" . $data . "\r\n---------------------------------------------------------------\r\n", FILE_APPEND);
    }
}
/**
 *  支付扫码页面
 */
if(!(function_exists('get_pc_html'))){
    function get_pc_html($img,$out_trade_no,$amount,$type='支付宝'){
        echo '<!doctype html><html><head><meta charset="utf-8"><title>'.$type.'</title></head>
        <body style="background:#f8f8f8;margin:0;padding:0;">
            <div style="height:50px;width:100%;background: white;margin-bottom: 2%;border-bottom: 2px solid #0da4ea"> 
            <span style="line-height: 50px;margin-left: 20%;">订单编号：'.$out_trade_no.' </span>
            <span style="float:right;margin-right: 30%;line-height: 50px;"> <i style="color:#ff5722eb;font-style:normal;font-weight:900;">'.abs($amount).'</i> 元</span></div>
            <div class="content" style="background: white;width:50%;margin:auto;padding:1% 0 1% 0;text-align:center;">
                <span style="display:block;margin-bottom: 15px;">扫一扫付款 <em style="color:#ff5722eb;font-style:normal;font-weight:900;"> '.abs($amount).' </em> 元 </span>
                <p><img src="data:image/png;base64,'.$img.'" style="width:200px;height:200px;border:2px solid #0da4ea;" /></p>
                <div><img src="/saoyisao.png" /><span style="position: relative;top: -8px;color:#FF5722;font-weight: 600;">&nbsp;打开'.$type.'，扫一扫付款</span></div>
                <span style="width: 100px;display: block;margin-left: 100px;font-weight: 900;color: red;"> 温馨提示：</span>
                    <p style="margin-left:110px;text-align:left;">1、发起金额和支付宝金额请保持一致，否则不会到账 </p>
                    <p style="margin-left:110px;text-align:left;">2、请不要修改备注，修改备注将不会到账 </p>
                    <p style="margin-left:110px;text-align:left;">3、未通过网页发起充值和直接使用支付宝转账的行为将不会到账 </p>
                    <p style="margin-left:110px;text-align:left;">4、支付宝提示补全姓名时，请参考支付宝昵称 </p>
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
            <script>
            $(document).ready(function () {
                var r = window.setInterval(function () {
                    $.ajax({
                        type: "POST",
                        url: "/pay/order_query",
                        data:"system_no='.$out_trade_no.'",
                        success: function (result) {
                            if (result.code == "success") {
                                clearInterval(r);
                                window.href=result.data.url;
                            }
                        }
                    });
                }, 5000);
            });
        </script>
        </body>
        </html>';
    }
}

