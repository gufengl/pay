<?php

/**
 * 用法：
 * class index
 * {
 *     use \traits\controller\Jump;
 *     public function index(){
 *         $this->error();
 *         $this->redirect();
 *     }
 * }
 */
namespace traits\controller;

use think\Container;
use think\exception\HttpResponseException;
use think\Response;
use think\response\Redirect;

trait Jump
{
    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  mixed     $data 返回的数据
     * @param  integer   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function success($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
    {
        
        if(!request()->isAjax()){
            $html = ' <link href="/static/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">';
            $html .= '<script src="/static/js/jquery.min.js"></script>';
            $html .= '<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>';
            if(empty($url)){
                $html .= '<script>
                    $(function(){
                        swal({
                            title:"'.$msg.'",
                            timer: "'.($wait*1000).'",
                            type:"success",
                            showConfirmButton: false,
                        })
                        var index = parent.layer.getFrameIndex(window.name);
                        if(index == null){
                            setTimeout(function() {
                                window.history.go(-1);
                            }, 2000)
                        }else{
                            setTimeout(function() {
                                parent.layer.close(index);
                            }, "'.($wait*1000).'") 
                        }
                    })
                </script>';
            }else{
                $html .= '<script>
                    $(function(){
                        swal({
                            title:"'.$msg.'",
                            timer: "'.($wait*1000).'",
                            type:"success",
                            showConfirmButton: false,
                        })
                        setTimeout(function() {
                            window.location.href="'.$url.'";
                        }, "'.($wait*1000).'")
                    })
                </script>';
            }
           exit($html);
        }else{

            if (is_null($url) && isset($_SERVER["HTTP_REFERER"])) {
                $url = $_SERVER["HTTP_REFERER"];
            } elseif ('' !== $url) {
                $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : Container::get('url')->build($url);
            }

            $result = [
                'code' => 1,
                'msg'  => $msg,
                'data' => $data,
                'url'  => $url,
                'wait' => $wait,
            ];

            $type = $this->getResponseType();
            // 把跳转模板的渲染下沉，这样在 response_send 行为里通过getData()获得的数据是一致性的格式
            if ('html' == strtolower($type)) {
                $type = 'jump';
            }

            $response = Response::create($result, $type)->header($header)->options(['jump_template' => $this->app['config']->get('dispatch_success_tmpl')]);

            throw new HttpResponseException($response);
        }
    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  mixed     $data 返回的数据
     * @param  integer   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function error($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
    {
        if(!request()->isAjax()){
            $html = ' <link href="/static/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">';
            $html .= '<script src="/static/js/jquery.min.js"></script>';
            $html .= '<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>';
            if(empty($url)){
                $html .= '<script>
                    $(function(){
                        swal({
                            title:"'.$msg.'",
                            timer: "'.($wait*1000).'",
                            type:"error",
                            showConfirmButton: false,
                        })
                        var index = parent.layer.getFrameIndex(window.name);
                        if(index == null){
                            setTimeout(function() {
                                window.history.go(-1);
                            }, 2000)
                        }else{
                            setTimeout(function() {
                                parent.layer.close(index);
                            }, "'.($wait*1000).'") 
                        }
                    })
                </script>';
            }else{
                $html .= '<script>
                    $(function(){
                        swal({
                            title:"'.$msg.'",
                            timer: "'.($wait*1000).'",
                            type:"success",
                            showConfirmButton: false,
                        })
                        setTimeout(function() {
                            window.location.href="'.$url.'";
                        }, "'.($wait*1000).'")
                    })
                </script>';
            }
           exit($html);
        }else{
            
            $type = $this->getResponseType();
            if (is_null($url)) {
                $url = $this->app['request']->isAjax() ? '' : 'javascript:history.back(-1);';
            } elseif ('' !== $url) {
                $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : $this->app['url']->build($url);
            }
    
            $result = [
                'code' => 0,
                'msg'  => $msg,
                'data' => $data,
                'url'  => $url,
                'wait' => $wait,
            ];
    
            if ('html' == strtolower($type)) {
                $type = 'jump';
            }
    
            $response = Response::create($result, $type)->header($header)->options(['jump_template' => $this->app['config']->get('dispatch_error_tmpl')]);
    
            throw new HttpResponseException($response);
        } 

    }
    
    /**
     * 返回封装后的API数据到客户端
     * @access protected
     * @param  mixed     $data 要返回的数据
     * @param  integer   $code 返回的code
     * @param  mixed     $msg 提示信息
     * @param  string    $type 返回数据格式
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function result($data, $code = 0, $msg = '', $type = '', array $header = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => time(),
            'data' => $data,
        ];

        $type     = $type ?: $this->getResponseType();
        $response = Response::create($result, $type)->header($header);

        throw new HttpResponseException($response);
    }

    /**
     * URL重定向
     * @access protected
     * @param  string         $url 跳转的URL表达式
     * @param  array|integer  $params 其它URL参数
     * @param  integer        $code http code
     * @param  array          $with 隐式传参
     * @return void
     */
    protected function redirect($url, $params = [], $code = 302, $with = [])
    {
        $response = new Redirect($url);

        if (is_integer($params)) {
            $code   = $params;
            $params = [];
        }

        $response->code($code)->params($params)->with($with);

        throw new HttpResponseException($response);
    }

    /**
     * 获取当前的response 输出类型
     * @access protected
     * @return string
     */
    protected function getResponseType()
    {
        if (!$this->app) {
            $this->app = Container::get('app');
        }

        $isAjax = $this->app['request']->isAjax();
        $config = $this->app['config'];

        return $isAjax
        ? $config->get('default_ajax_return')
        : $config->get('default_return_type');
    }
}
