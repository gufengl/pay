function modaldemoBank(id) {
    if(id != null){
        $.post('/manage/save_bank',{id:id,type:1},function(result){
            if(result.code == 1){
                $("input[name='p_id']").val(result.data.id);
                $("input[name='card_number']").val(result.data.card_number);
                $("input[name='account_name']").val(result.data.account_name);
                $("input[name='bank_name']").val(result.data.bank_name);
                $("input[name='branch']").val(result.data.branch);
                //初始化插件数据
                $('#distpicker').distpicker('destroy');
                //重新赋值
                $('#distpicker').distpicker({
                    province: result.data.province,
                    city: result.data.city,
                    district: result.data.district
                });
            }else{
                parent.layer.msg(result.msg);
                return false;
            }
        })
    }else{
        $("input[name='p_id']").val('');
        $("input[name='card_number']").val('');
        $("input[name='account_name']").val('');
        $("input[name='bank_name']").val('');
        $("input[name='branch']").val('');
        $('#distpicker').distpicker('destroy');
        $('#distpicker').distpicker();
    }
    $("#modal-demo-bank").modal("show");
}
//添加银行卡
$(".btn-bank").click(function(){
    var p_id = $("input[name='p_id']").val();
    var card_number = $("input[name='card_number']").val();
    var account_name = $("input[name='account_name']").val();
    var bank_name = $("input[name='bank_name']").val();
    var branch = $("input[name='branch']").val();
    var province = $("#province option:selected").val();
    var city = $("#city option:selected").val();
    var district = $("#district option:selected").val();
    if(card_number == ''){
        parent.layer.msg("银行卡号不能为空");
        return false;
    }
    if(account_name == ''){
        parent.layer.msg("银开户名不能为空");
        return false;
    }
    if(bank_name == ''){
        parent.layer.msg("所属银行不能为空");
        return false;
    }
    if(branch == ''){
        parent.layer.msg("所在支行不能为空");
        return false;
    }
    if(province == '' || city == '' || district == ''){
        parent.layer.msg("开卡地区不能为空");
        return false;
    }
    var data = {};
    data['p_id'] = p_id;
    data['card_number'] = card_number;
    data['account_name'] = account_name;
    data['bank_name'] = bank_name;
    data['branch'] = branch;
    data['location'] = province+' - '+city+' - '+district;
    $(".btn-bank").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});
    $.post('/manage/save_bank',data,function(result){
        parent.layer.msg(result['msg']);
        parent.layer.close(index);//去掉加载动画
        if(result['code'] == 1){
            $("#modal-demo").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".btn-bank").removeAttr("disabled");
        }
    });
})
//删除银行卡
$(".bt-bank-del").click(function(){
    var id = $(this).attr('data-id');
    parent.layer.confirm('确认删除该银行卡吗？', {
        btn: ['确定','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        $.post('/manage/del_bank',{bank_id:id},function(result){
            parent.layer.msg(result['msg']);
            if(result.code == 1){
                setTimeout(function() {
                    window.location.reload();
                }, 1000)
            }
        });
    }, function(){
        parent.layer.msg('操作取消');
    });
})
//提现申请
$(".bt-apply").click(function(){
    
    var bank_id = $("#bank_id option:selected").val();
    var total_fee = $("input[name='total_fee']").val();
    if(bank_id == '0'){
        parent.layer.msg('至少选择一张银行卡');
        return false;
    }
    total_t = $("input[name='total_t']").attr("data-am");
    if(total_fee == ''){
        parent.layer.msg('金额不能为空');
        return false;
    }
    //金额判断
    if(parseFloat(total_fee) >= parseFloat(total_t)){
        parent.layer.msg('余额不足');
        return false;
    }
    //设置不可点击，避免重复提交
    $(".bt-apply").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});//添加加载动画
    $.post('/manage/withRequest',{bank_id:bank_id,total_fee:total_fee},function(result){
        parent.layer.msg(result.msg);
        parent.layer.close(index);
        if(result.code == '1'){
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".bt-apply").removeAttr("disabled");
        }
    });
})

//拒绝出款
$(".no_submit").click(function(){
    var remark = $("#remark").val();
    var wid = $("#wid").val();
    if(remark == ''){
        parent.layer.msg('拒绝理由不能为空');
        return false;
    }
    //设置不可点击，避免重复提交
    $(".o_submit").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});//添加加载动画
    $.post("/manage/operation",{wid:wid,type:'no',remark:remark},function(result){
        parent.layer.msg(result.msg);
        parent.layer.close(index);
        if(result.code == '1'){
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".no_submit").removeAttr("disabled");
        }
    });
})
//同意出款
function out_amount(id){

       
        layer.prompt({title: '请输入出款备注', formType: 2}, function(text, index){
           //设置不可点击，避免重复提交
            $(".bt-w").attr({ disabled: "disabled" });
            index = layer.load(0, {shade: false});//添加加载动画
            $.post("/manage/operation",{wid:id,text:text,type:'ru'},function(result){
                layer.msg(result.msg);
                layer.close(index);
                if(result.code == '1'){
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000)
                }else{
                    $(".bt-w").removeAttr("disabled");
                }
            });
        });

}