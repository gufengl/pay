function modaldemo(id) {
    $.post("/manage/ajax_select", {
        id: id
    }, function(result) {
        $("#select").html(result.select);
        if (id) {
            $(".modal-title").html("编辑账号");
            $("input[name='role_id']").val(result.info.id);
            $("input[name='user_name']").val(result.info.user_name);
            $("input[name='nickname']").val(result.info.nickname);
            $("input[name='password']").attr("placeholder", "为空表示不修改密码")
        } else {
            $("input[name='user_name']").val('');
            $("input[name='nickname']").val('');
            $("input[name='role_id']").val('');
            $(".modal-title").html("添加账号");
            $("input[name='password']").attr("placeholder", "请输入登录密码")
        }
    });
    $("#modal-demo").modal("show")
}
//添加/修改
$(".btn-account").click(function() {
    var user_name = $("input[name='user_name']").val();
    var password = $("input[name='password']").val();
    var nickname = $("input[name='nickname']").val();
    var role_id = $("input[name='role_id']").val();
    var pid = $(".m-b option:selected").val();
    if (user_name == '') {
        parent.layer.msg("账号不能为空");
        return false
    }
    if (!role_id) {
        if (password == '') {
            parent.layer.msg("密码不能为空");
            return false
        }
    }
    if (pid == '0') {
        parent.layer.msg("请选择角色");
        return false
    }
    if (nickname == '') {
        parent.layer.msg("昵称不能为空");
        return false
    }
    $.post("/manage/editAccount", {
        name: user_name,
        pwd: password,
        pid: pid,
        nickname:nickname,
        role_id: role_id
    }, function(result) {
        parent.layer.msg(result['msg']);
        if (result['code'] == 1) {
            $("#modal-demo").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }
    })
})
//删除
$(".bt-acc-del").click(function(){
    var id = $(this).attr("data-id");
    parent.layer.confirm('确定删除账号吗', {
        btn: ['删除','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        $.post("/manage/delAccount",{id:id},function(result){
            parent.layer.msg(result['msg']);
            if(result['code'] == 1){
                setTimeout(function() {
                    window.location.reload();
                }, 1000)
            } 
        });
    }, function(){
        parent.layer.msg('操作取消');
    });   
})
 //代理商 - 加/减金额
 $(".ag-balance-edit").click(function(){
    var agent_id = $("#agent_id").val();
    var status = $("input[name='type']:checked").val();
    var balance = $("input[name='balance']").val();
    var remark = $("#remark").val();
    if(balance == '' || balance == 0){
        parent.layer.msg("变更金额不不能为空");
        return false;
    }
    $(".ag-balance-edit").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});//添加加载动画
    $.post('/manage/agentAmount',{agent_id:agent_id,type:1,status:status,balance:balance,remark:remark},function(result){
        parent.layer.close(index);//去掉加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            $("#modal-demo-agent").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".ag-balance-edit").removeAttr("disabled");
        }
    });
})
//代理商 - 冻结/解冻金额
$(".ag-bt-balance-edit").click(function(){
    var agent_id = $("#agent_id").val();
    var do_type = $("input[name='do-type']:checked").val();
    var do_balance = $("input[name='do-balance']").val();
    var do_remark = $("#do-remark").val();
    if(do_balance == '' || do_balance == 0){
        parent.layer.msg("变更金额不不能为空");
        return false;
    }
    $(".ag-bt-balance-edit").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});//添加加载动画
    $.post('/manage/agentAmount',{agent_id:agent_id,type:2,status:do_type,balance:do_balance,remark:do_remark},function(result){
        parent.layer.close(index);//去掉加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            $("#modal-demo-agent").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".ag-bt-balance-edit").removeAttr("disabled");
        }
    });
})
//代理商 - 入金
$(".agent_rukuan_type").click(function(){
    var id = $(this).attr("tt");
    calss = $(this).find("i").attr("class");
    $.post("/manage/agentStatu",{agent_id:id,type:'rj'},function(result){
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            if(calss == 'fa fa-check'){
                $(".agent_rukuan_"+id).find("i").removeClass("fa fa-check").addClass("fa fa-ban").css("color","inherit").html("&nbsp;禁用");
            }else{
                $(".agent_rukuan_"+id).find("i").removeClass("fa fa-ban").addClass("fa fa-check").css("color","#1ab394").html("&nbsp;正常");
            }
        }
    })

});
//代理商 - 出款
$(".agent_chukuan_type").click(function(){
    var id = $(this).attr("tt");
    calss = $(this).find("i").attr("class");
    $.post("/manage/agentStatu",{agent_id:id,type:'ck'},function(result){
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            if(calss == 'fa fa-check'){
                $(".agent_chukuan_"+id).find("i").removeClass("fa fa-check").addClass("fa fa-ban").css("color","inherit").html("&nbsp;禁用");
            }else{
                $(".agent_chukuan_"+id).find("i").removeClass("fa fa-ban").addClass("fa fa-check").css("color","#1ab394").html("&nbsp;正常");
            }
        }
    })
});