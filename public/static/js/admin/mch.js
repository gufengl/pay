function modaldemo() {
    $("#modal-demo").modal("show");
}
function modalamount(id,balance,freeze) {
    $("#mch_id").val(id);
    $("#id-balance").val('可用：'+balance+'元   冻结：'+freeze+'元');
    $("#do-id-balance").val('冻结：'+freeze+'元   可用：'+balance+'元');
    
    $("#modal-amount").modal("show");
}
//添加商户
$(".btn-mch").click(function(){
    var user_name = $("input[name='user_name']").val();
    var password = $("input[name='password']").val();
    var agent = $("#agent option:selected").val();
    // var is_agent = $("#is_agent option:selected").val();
    if(user_name == '' && password == ''){
        parent.layer.msg("商户名称和密码不能为空");
        return false;
    }
    //设置不可点击，避免重复提交
    $(".btn-mch").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});

    $.post("/manage/addMch",{name:user_name,pwd:password,agent:agent,is_agent:1},function(result){
        parent.layer.msg(result['msg']);
        parent.layer.close(index);//去掉加载动画
        if(result['code'] == 1){
            $("#modal-demo").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".btn-mch").removeAttr("disabled");
        }
    });
});
//添加代理
$(".btn-agent").click(function(){
    var user_name = $("input[name='user_name']").val();
    var password = $("input[name='password']").val();
    var agent_rate = $("input[name='agent_rate']").val();
    if(user_name == '' && password == '' && agent_rate == ''){
        parent.layer.msg("商户名称、密码和代理费率不能为空");
        return false;
    }
    $(".btn-agent").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});
    $.post("/manage/addMch",{name:user_name,pwd:password,agent_rate:agent_rate,is_agent:2},function(result){
        parent.layer.msg(result['msg']);
        parent.layer.close(index);//去掉加载动画
        if(result['code'] == 1){
            $("#modal-demo").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".btn-agent").removeAttr("disabled");
        }
    });
})
//加/减金额
$(".balance-edit").click(function(){

    var mch = $("#mch_id").val();
    var status = $("input[name='type']:checked").val();
    var balance = $("input[name='balance']").val();
    var remark = $("#remark").val();
    if(balance == '' || balance == 0){
        parent.layer.msg("变更金额不不能为空");
        return false;
    }
    //设置不可点击，避免重复提交
    $(".balance-edit").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});
    $.post("/manage/change_balance",{mch_id:mch,type:1,status:status,balance:balance,remark:remark},function(result){
        parent.layer.close(index);//去掉加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            $("#modal-amount").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".balance-edit").removeAttr("disabled");
        }
    });
});
//冻结/解冻
$(".do-balance-edit").click(function(){
    
    var mch_id = $("#mch_id").val();
    var do_type = $("input[name='do-type']:checked").val();
    var do_balance = $("input[name='do-balance']").val();
    var do_remark = $("#do-remark").val();
    if(do_balance == '' || do_balance == 0){
        parent.layer.msg("变更金额不不能为空");
        return false;
    }
    //设置不可点击，避免重复提交
    $(".do-balance-edit").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});//添加加载动画
    $.post("/manage/change_balance",{mch_id:mch_id,type:2,status:do_type,balance:do_balance,remark:do_remark},function(result){
        parent.layer.close(index);//去掉加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            $("#modal-amount").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".do-balance-edit").removeAttr("disabled");
        }
    });
});
//配置渠道
$(".bt-mch-p").click(function(){
    $.ajax({
        type: "POST",//方法类型
        url: "/manage/configChannel" ,//url
        data: $('#product').serialize(),
        beforeSend:function(data){
            $(".bt-mch-p").attr({ disabled: "disabled" });
            index = parent.layer.load(0, {shade: false});//添加加载动画
        },
        success: function (result) {
            parent.layer.msg(result['msg']); 
            if(result['code'] == 1){
                setTimeout(function() {
                    window.location.reload();
                }, 1000)
            }
        },
        error : function() {
            alert("异常！");
        },
        complete: function () {
            parent.layer.close(index);//去掉加载动画 
            $(".bt-mch-p").removeAttr("disabled");
        }
    });
})
//删除商户
$(".bt-mch-del").click(function(){
    var id = $(this).attr("data-id");
    parent.layer.confirm('删除后，商户的所有数据将无法找回，确认要操作吗？', {
        btn: ['删除','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        $.post("/manage/mchDelete",{mch_id:id},function(result){
            parent.layer.msg(result.msg);
            if(result.code == 1){
                setTimeout(function() {
                    window.location.reload();
                }, 1000)
            }
        });
    }, function(){
        parent.layer.msg('操作取消');
    });
})