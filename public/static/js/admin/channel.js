function modaldemo() {

    $("#modal-demo").modal("show");
}
$(document).ready(function () {
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
});
//将第一个字符改成大写
function jsUcfirst() {
    var str = $("input[name='channel_class']").val();

	var channel_class = str.substr(0, 1).toUpperCase() + str.substr(1);
    
    $("input[name='channel_class']").val(channel_class);
}


//添加渠道
$(".btn-channel").click(function(){
   
    name = $("input[name='channel_name']").val();
    channel_class = $("input[name='channel_class']").val();
    white_ip = $("#white_ip").val();
    status = $("input[name='status']:checked").val();
    if(!name || name =='0'){
        parent.layer.msg("渠道名称不能为空");
        return false;
    }
    if(!channel_class || channel_class == '0'){
        parent.layer.msg("渠道类名不能为空");
        return false;
    }
    if(white_ip == ''){
        parent.layer.msg("渠道白名单不能为空");
        return false;
    }
    $(".btn-channel").attr({ disabled: "disabled" });
    index = parent.layer.load(0, {shade: false});
    var data = {};
    data['name'] = name;
    data['white_ip'] = white_ip;
    data['channel_class'] = channel_class;
    data['status'] = status;
    $.post("/manage/addChannel",data,function(result){
        parent.layer.close(index);//去掉加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            $("#modal-demo").modal("hide");
            setTimeout(function() {
                window.location.reload();
            }, 1000)
        }else{
            $(".btn-channel").removeAttr("disabled");
        }
    })
});
//添加支付类型
$(".btn-payment").click(function(){
   
    var channel_id = $("#channel_id option:selected").val();
    var pay_id = $("input[name='pay_id']").val();
    var qualified = $("input[name='qualified']").val();
    var payment = $("#payment option:selected").val();
    var channel_code = $("input[name='channel_code']").val();
    var cost_rate = $("input[name='cost_rate']").val();
    var run_rate = $("input[name='run_rate']").val();
    var min_amount = $("input[name='min_amount']").val();
    var max_amount = $("input[name='max_amount']").val();
    var fixed_amount = $("input[name='fixed_amount']").val();
    var double_amount = $("input[name='double_amount']").val();
    var exclude_tail = $("input[name='exclude_tail']").val();
    if(channel_id == 0){
        parent.layer.msg("请选择渠道");
        return false;
    }
    if(payment == 0){
        parent.layer.msg("请选择支付类型");
        return false;
    }
    if(channel_code == ''){
        parent.layer.msg("渠道编码不能为空");
        return false;
    }
    if(cost_rate == ''){
        parent.layer.msg("成本费率不能为空");
        return false;
    }
    if(run_rate == ''){
        parent.layer.msg("运营费率不能为空");
        return false;
    }
    if(run_rate < cost_rate){
        parent.layer.msg("运营费率不能小于成本费率");
        return false;
    }
    var data = {};
    data['pay_id'] = pay_id;
    data['qualified'] = qualified;
    data['channel_id'] = channel_id;
    data['payment'] = payment;
    data['channel_code'] = channel_code;
    data['cost_rate'] = cost_rate;
    data['run_rate'] = run_rate;
    data['min_amount'] = min_amount;
    data['max_amount'] = max_amount;
    data['fixed_amount'] = fixed_amount;
    data['double_amount'] = double_amount;
    data['exclude_tail'] = exclude_tail;
    $(".btn-payment").attr({ disabled: "disabled" });//防止重复提交
    index = parent.layer.load(0, {shade: false});//加载动画
    var iframe = parent.layer.getFrameIndex(window.name);
    $.post("/manage/addPayment",data,function(result){
        parent.layer.close(index);//关闭加载动画
        parent.layer.msg(result['msg']);
        if(result['code'] == 1){
            parent.layer.close(iframe); //关闭iframe层
        }else{
            $(".btn-payment").removeAttr("disabled");
        }
    });
})
//删除支付类型
$(".bt-pay-del").click(function(){
    id = $(this).attr("data-id");
    parent.layer.confirm('确认要删除吗？', {
        btn: ['删除','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        $.post('/manage/delPayment',{pid:id},function(result){
            parent.layer.msg(result['msg']);
            setTimeout(function() {
                window.location.reload();
            })
        });
    }, function(){
        parent.layer.msg('操作取消');
    });
});
//删除支付渠道
$(".bt-ch-del").click(function(){
    id = $(this).attr("data-id");
    parent.layer.confirm('确认要删除吗？', {
        btn: ['删除','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        $.post('/manage/delChan',{cid:id},function(result){
            parent.layer.msg(result['msg']);
            if(result['code'] == 1){
                setTimeout(function() {
                    window.location.reload();
                })
            }
        });
    }, function(){
        parent.layer.msg('操作取消');
    });
});