//补发通知
$(".bt-notice").click(function(){
    id = $(this).attr('data-id');
    $.post("/manage/notice",{oid:id},function(result){
        parent.layer.msg(result.msg);
        if(result.code == 1){
            if(result.data.notice == 2){
                $(".notice_"+id).html('<span style="color: #1ab394;">已回调</span>');
            }
        }
    });
});
//强制入账
$(".bt-compel").click(function(){
    id = $(this).attr('data-id');
    $(".compel").attr({ disabled: "disabled" });
    
   //询问框
    parent.layer.confirm('确定要执行此操作？', {
        btn: ['确定','取消'], //按钮
        shade: false //不显示遮罩
    }, function(){
        index = parent.layer.load(0, {shade: false});
        $.post("/manage/compel",{oid:id},function(result){
            parent.layer.close(index);
            parent.layer.msg(result.msg);
            if(result.code == 1){
                setTimeout(function() {
                    window.location.reload();
                }, 1000)
            }
            $(".bt-compel").removeAttr("disabled");
        });
    }, function(){
       
        parent.layer.msg('操作取消');
    });
})