<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>支付在线测试</title>
	<link href="./static/ceshi/index.css" rel="stylesheet"></head>
    <body>
        <div class="tastesdk-box">
            <div class="header clearfix">
                <div class="title">
                    <p class="logo">
                        <span>收银台</span></p>
                    <div class="right">
                        <div class="clearfix">
                            <ul class="clearfix"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main">
                <div class="typedemo">
                    <div class="demo-pc">
                        <div class="pay-jd">
                            <form action="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/pay/test';?>" method="post" autocomplete="off" target="_blank">
                                
                                <div class="two-step">
                                    <p>
                                        <strong>请您及时付款，以便订单尽快处理！</strong>
                                        
                                    <ul class="pay-infor">
                                        <li>商品名称：测试应用-支付功能体验(非商品消费)</li>
                                        <li>支付金额：
                                            <strong>
                                            <input type="text" onkeyup="value=value.replace(/^\D*(\d*(?:\.\d{0,3})?).*$/g, '$1')" name="amount" value="100">
                                            <span>元</span></strong>
                                        </li>
                                    </ul>
                                    <h5>选择支付方式：</h5>
                                    <ul class="pay-label">
                                        <li>
                                            <input value="aliqr" checked="checked" name="channel" id="zfb" type="radio">
                                            <label for="zfb">
                                                <img src="./static/ceshi/zhifubao.png" alt="支付宝">
                                                <span>支付宝扫码</span></label>
                                        </li>
                                        <li>
                                            <input value="aliwap" name="channel" id="zfb1" type="radio">
                                            <label for="zfb1">
                                            <img src="./static/ceshi/zhifubao.png" alt="支付宝">
                                            <span>支付宝H5</span></label>
                                        </li>
                                        <li>
                                            <input value="wx" name="channel" id="wx" type="radio">
                                            <label for="wx">
                                            <img src="./static/ceshi/weixin.png" alt="微信支付">
                                            <span>微信扫码</span></label>
                                        </li>
                                        <li>
                                            <input value="wxwap" name="channel" id="wxh5" type="radio">
                                            <label for="wxh5">
                                            <img src="./static/ceshi/weixin.png" alt="微信支付">
                                            <span>微信H5</span></label>
                                        </li>
                                        <li>
                                            <input value="flash" name="channel" id="flash" type="radio">
                                            <label for="flash">
                                            <img src="./static/ceshi/weixin.png" alt="云闪付">
                                            <span>云闪付</span></label>
                                        </li>
                                        <li>
                                            <input value="quick" name="channel" id="kuaijie" type="radio">
                                            <label for="kuaijie">
                                            <img src="./static/ceshi/yinlian.png" alt="快捷支付">
                                            <span>快捷支付</span></label>
                                        </li>
                                        <li>
                                            <input value="unionpay" name="channel" id="bankqr" type="radio">
                                            <label for="bankqr">
                                            <img src="./static/ceshi/yinlian.png" alt="银联扫码">
                                            <span>银联扫码</span></label>
                                        </li>
                                    </ul>
                                    <style>.plist p{float:left;margin-left:25px;margin-top:10px;border:1px solid #ccc; width:150px;} .plist p.current{border:1px solid #E43D40} #footer{background:#263445;text-align:center;color:#8392A7;margin-top:30px;padding:20px }</style>
                                    <script src="./static/ceshi/jquery.min.js"></script>
                                    <script>$(function() {
                                            $('.plist p').click(function() {
                                                $('.plist p').removeClass('current');
                                                $(this).addClass('current');
                                                $('[name=bankCode]').val($(this).find('img').attr('data-pid'));
                                            });
                                            $(".pay-label li").click(function() {
                                                var code = $(this).find("input[name='channel']").val();
                                                if (code == 'unionpay') {
                                                    $(".plist").show();
                                                } else {
                                                    $(".plist").hide();
                                                }
                                            })
                                        })</script>
                                    <div>
                                        <input type="hidden" name="bankCode" value="ICBC">
                                        <div class="plist banklist" style="display: none;">
                                            <p class="current">
                                                <img src="./static/ceshi/ICBC.gif" data-pid="ICBC"></p>
                                            <p>
                                                <img src="./static/ceshi/ABC.gif" data-pid="ABC"></p>
                                            <p>
                                                <img src="./static/ceshi/BOCSH.gif" data-pid="BOCSH"></p>
                                            <p>
                                                <img src="./static/ceshi/CCB.gif" data-pid="CCB"></p>
                                            <p>
                                                <img src="./static/ceshi/CMB.gif" data-pid="CMB"></p>
                                            <p>
                                                <img src="./static/ceshi/SPDB.gif" data-pid="SPDB"></p>
                                            <p>
                                                <img src="./static/ceshi/GDB.gif" data-pid="GDB"></p>
                                            <p>
                                                <img src="./static/ceshi/BOCOM.gif" data-pid="BOCOM"></p>
                                            <p>
                                                <img src="./static/ceshi/PSBC.gif" data-pid="PSBC"></p>
                                            <p>
                                                <img src="./static/ceshi/CNCB.gif" data-pid="CNCB"></p>
                                            <p>
                                                <img src="./static/ceshi/CMBC.gif" data-pid="CMBC"></p>
                                            <p>
                                                <img src="./static/ceshi/CEB.gif" data-pid="CEB"></p>
                                            <p>
                                                <img src="./static/ceshi/CIB.gif" data-pid="CIB"></p>
                                            <p>
                                                <img src="./static/ceshi/BOS.gif" data-pid="BOS"></p>
                                            <p>
                                                <img src="./static/ceshi/PAB.gif" data-pid="PAB"></p>
                                            <p>
                                                <img src="./static/ceshi/BCCB.gif" data-pid="BCCB"></p>
                                            <div style="clear:left"></div>
                                        </div>
                                    </div>
                                    <div class="btns">
                                        <button type="submit" class="pcdemo-btn sbpay-btn">立即支付</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>
</html>