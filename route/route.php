<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
Route::get('/', function () {
   return 'Access error';
});

//API接口路由
Route::group('pay', function(){

    Route::post('gateway','api/pay/index');//商户对接下单

    Route::get('transit/[:pid]','api/pay/transit');//支付中转

    Route::rule('notify/[:n_code]','api/pay/notify');//异步回调

    Route::rule('callback','api/pay/callback');//同步跳转
    
    Route::post('order_query','api/pay/order_query');//订单状态

    Route::post('test','api/test/index');//支付测试

    Route::post('test/notify','api/test/notify');//测试回调

    Route::post('test/callback','api/test/callback');//测试同步跳转

});
//商户后台路由
Route::group('web', function(){
    Route::get('login','web/login/index');//后台登录页面  
    Route::post('dologin','web/login/dologin');//登录处理
    Route::get('login/logout','web/login/logout');//退出登录
    Route::post('google','web/login/google');//绑定股谷歌
});
//商户路由中间件
Route::group('web', function(){
    Route::get('/','web/login/index');//后台登录页面  
    Route::get('home','web/index/index');//首页
    Route::get('welcome','web/index/welcome');//欢迎页
    Route::rule('order','web/order/index');//交易记录
    Route::rule('water','web/order/waterRecord');//资金变更
    Route::rule('agent_water','web/order/waterAgentRecord');//代理资金变更
    Route::rule('withdrawal','web/user/withdrawal');//提现记录
    Route::rule('bank','web/user/bank_card');//银行卡
    Route::get('material','web/user/material');//商户信息
    Route::post('pwd','web/user/up_pwd');//修改密码
    Route::get('document','web/system/document');//对接文档
    Route::post('withRequest','web/user/withRequest');//提现申请
    Route::get('bankcard','web/user/bankCard');//银行卡
    Route::post("save_bank",'web/user/saveBank');//添加/编辑
    Route::post("del_bank",'web/user/bankDelete');//删除
    Route::post("reset_md5key",'web/user/reset_md5key');//重置秘钥
    Route::get("user",'web/user/index');//下级商户
    Route::post("user/is_mch",'web/user/is_mch');//更新商户状态、入金、出款
    Route::rule("user/configChannel/[:id]/[:pid]",'web/user/configChannel');//配置商户渠道
})->middleware("Merchants");

//后台路由
Route::group('manage', function(){
    Route::get('login','manage/login/index');//后台登录页面   
    // Route::get('verify','manage/login/verify');//获取验证码
    Route::post('checkLogin','manage/login/checkLogin');//登录验证
    Route::get('login/logout','manage/login/logout');//退出登录
    Route::post('ajax_select','manage/system/ajax_select');//获取角色
    
    Route::post('google','manage/home/google');//绑定谷歌
    Route::post('pwd','manage/system/up_pwd');//修改密码
});

//后台路由中间件
Route::group('manage', function(){
    Route::get('/','manage/home/index');//首页
    Route::get('home','manage/home/index');//首页
    Route::get('welcome','manage/home/welcome');//欢迎页
    Route::post('cache','manage/home/webCache');//清除缓存
    // +----------------------------------------------------------------------
    // | 商户管理
    // +----------------------------------------------------------------------
    Route::rule("user",'manage/user/index','GET|POST');//商户列表
    Route::get("google",'manage/user/google');//绑定谷歌验证器
    Route::rule("user/agent",'manage/user/agent','GET|POST');//代理商列表
    Route::post("statusMch",'manage/user/statusMch');//状态、出款、入金
    Route::post("addMch",'manage/user/addMch');//添加商户
    Route::post("mchDelete",'manage/user/mch_delete');//删除商户
    Route::rule("mchEdit/[:pid]/[:id]",'manage/user/editMch');//编辑/修改
    Route::post("change_balance",'manage/user/changeAmount');//加/减/冻结/解冻余额
    Route::get("detail",'manage/user/detail');//代理商收益明细
    Route::rule("record",'manage/user/mch_record','GET|POST');//资金变更记录
    Route::rule("configChannel/[:pid]/[:mid]",'manage/user/mchChannel');//配置渠道
    Route::rule("agentConf/[:id]/[:mid]",'manage/user/agentConf');//配置代理渠道
    // +----------------------------------------------------------------------
    // | 系统管理
    // +----------------------------------------------------------------------
    Route::get("role",'manage/system/role');//角色列表
    Route::rule("roleEdit/[:pid]/[:id]",'manage/system/role_details');//编辑
    Route::get("account",'manage/system/system_account');//管理账号
    Route::post("editAccount",'manage/system/save_account');//编辑
    Route::post("account_status",'manage/system/account_status');//账号状态
    Route::post("delAccount",'manage/system/acc_delete');//删除账号
    Route::rule("operationLog",'manage/system/log');//操作记录
    Route::rule("menu",'manage/system/menu');//菜单
    Route::post("system/menu_type",'manage/system/menuType');//菜单状态
    Route::get("meunDel/pid/:id",'manage/system/meunDel')->pattern(['id'=>'\d+']);//菜单删除
    Route::post("menuSort",'manage/system/menuSort');//菜单排序
    Route::post("add_menu",'manage/system/addMenu');//添加菜单
    Route::rule("menu_edit/:pid/:id",'manage/system/menuEdit');//编辑菜单
    Route::get("mchLog",'manage/system/mchLog');//商户日
    Route::post("maintain",'manage/system/maintain');//网站维护
    // +----------------------------------------------------------------------
    // | 出款管理
    // +----------------------------------------------------------------------
    Route::rule('with','manage/withdrawal/index','GET|POST');//提现记录
    Route::post('withRequest','manage/withdrawal/withRequest');//提现申请
    Route::post('operation','manage/withdrawal/with_operation');//出款/拒绝
    Route::get('bankcard','manage/withdrawal/bankCard');//银行卡
    Route::post("save_bank",'manage/withdrawal/saveBank');//添加/编辑
    Route::post("del_bank",'manage/withdrawal/bankDelete');//删除
    Route::post("wit_is_lock",'manage/withdrawal/isLock');//锁定/解除
    Route::post("chanEdit",'manage/withdrawal/chan_edit');//编辑渠道
    
    // +----------------------------------------------------------------------
    // | 渠道管理
    // +----------------------------------------------------------------------
    Route::rule("channel",'manage/channel/index','GET|POST');//渠道列表
    Route::rule("payment",'manage/channel/payType');//支付类型
    Route::rule("addPayment/[:pid]/[:tid]",'manage/channel/addType');//添加支付类型
    Route::post("addChannel",'manage/channel/addChannel');//添加渠道
    Route::rule("editChannel/[:pid]/[:cid]",'manage/channel/channel_edit');//编辑渠道
    Route::post("channelState",'manage/channel/channel_status');//渠道状态
    Route::post("stateType",'manage/channel/typeStart');//支付类型状态、结算方式
    Route::post("delPayment",'manage/channel/del_payment');//删除支付方式
    Route::post("delChan",'manage/channel/del_channel');//删除渠道
    // +----------------------------------------------------------------------
    // | 订单管理
    // +----------------------------------------------------------------------
    Route::rule("order",'manage/order/index','GET|POST');//订单列表
    Route::get("orderDetails/:id/[:oid]",'manage/order/details');//订单详情
    Route::post("notice",'manage/order/notice');//补发通知
    Route::post("compel",'manage/order/compel');//强制入账
    // +----------------------------------------------------------------------
    // | 数据统计
    // +----------------------------------------------------------------------
    Route::rule("chanStatistical",'manage/order/chan_statistical');//渠道统计
    Route::rule("mchStatistical",'manage/order/mch_statistical');//商户统计
    Route::rule("platformStatistical",'manage/order/platformh_statistical');//平台统计
    // +----------------------------------------------------------------------
    // | IP管理
    // +----------------------------------------------------------------------
    Route::get("blacklist",'manage/system/black_list');//IP黑名单
    Route::post("blacklist_add",'manage/system/blacklistSave');//添加/删除
    Route::get("with_blacklist",'manage/system/with_blacklist');//商户下单
    Route::get("withdrawal_ip",'manage/system/withdrawal_ip');//商户出款
    Route::get("mch_ip_edit/pid/:mid",'manage/system/mch_ip_edit');//编辑下单ip
    Route::get("with_ip_edit/pid/:mid",'manage/system/with_ip_edit');//编辑出款ip
    Route::post("mch_ip_save",'manage/system/mch_ip_save');//添加/更新/删除(商户下单ip)
    Route::post("withdrawal_ip_save",'manage/system/withdrawal_ip_save');//出款

})->middleware("Manage");